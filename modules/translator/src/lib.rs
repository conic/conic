#![doc = include_str!("../README.md")]

use std::collections::HashMap;
use reqwest::IntoUrl;
use json::object;
use lang::tok::{Token, TokType};

const API_KEY: &str = env!("LIBRE_TRANSLATE_API");

pub async fn translate<T: IntoUrl>(toks: &mut Vec<Token>, api_path: T, source_lang: &str, target_lang: &str) -> reqwest::Result<()> {
	let mut api_path = api_path.into_url()?;
	api_path = api_path.join("translate").unwrap();

	let mut txt = String::new();
	// let mut text_positions = Vec::new();
	for (i, t) in toks.iter().enumerate() {
		if let TokType::Comment(lang, val) = &t.tt {

		}
	}

	let client = reqwest::Client::new();
	let map = HashMap::from([
		("q", ""),
		("source", source_lang), ("target", target_lang), ("format", "text"),
		("api_key", API_KEY)
	]);
	let resp = client.post(api_path).json(&map).send().await?;

	Ok(())
}

#[test]
fn check_path() -> Result<(), reqwest::Error> {
	let api_path = env!("LIBRE_TRANSLATE_URL");

	let client = reqwest::blocking::Client::new();
	let map = HashMap::from([
		("q", "hur mär du idag?"),
		("source", "sv"), ("target", "en"), ("format", "text"),
		("api_key", API_KEY)
	]);
	let resp = client.post(format!("{}/translate", api_path)).json(&map).send()?;
	println!("{:?}", resp);
	Ok(())
}
