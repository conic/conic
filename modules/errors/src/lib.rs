use std::fmt::{Display, Formatter};
use std::path::{Path, PathBuf};
use lang::prelude::LanguageRaw;
use lang::tok::Position;

/// Compilation error
///
/// Compilation error struct. Contains an error level, code, and position
pub struct Error {
    /// Error level. See [ErrorLevel] for more info
    err_level: ErrorLevel,
    /// Error code. See [ErrorCode] for more info
    code: ErrorCode,
    /// Error position. See [ErrorPosition] for more info
    pos: ErrorPosition,
}

/// Error level. This separates errors into groups that can be suppressed when building.
pub enum ErrorLevel {
    /// Cannot be suppressed. This causes compilation failure
    Error,
    /// Can be suppressed. These are fairly large non-fatal issues such as extensions not being used. Can be configured
    Warning,
    /// Can be suppressed. These are generally minor alterations such as a variable not being used or trailing
    /// semicolons. Can be configured
    Info,
}

pub enum ErrorOrWarning {
    Error {
        code: ErrorCode,
        position: ErrorPosition
    },
    Warning {
        code: ErrorCode,
        position: ErrorPosition
    }
}

pub struct Result {
    pub err_warn: Vec<ErrorOrWarning>
}

pub struct ErrorCode {
    type_code: u8,
    specifier: u8
}

pub struct ErrorPosition {
    pub file: PathBuf,
    pub pos_start: Position,
    pub pos_end: Position,
}

impl ErrorOrWarning {
    pub fn error<E: Into<ErrorCode>, F: Into<ErrorPosition>>(code: E, data: F) -> Self {
        Self::Error {
            code: code.into(),
            position: data.into()
        }
    }
    
    pub fn warning<E: Into<ErrorCode>, F: Into<ErrorPosition>>(code: E, data: F) -> Self {
        Self::Warning {
            code: code.into(),
            position: data.into()
        }
    }
}

impl ErrorOrWarning {
    fn to_string(&self, l: &LanguageRaw) -> String {
        #[cfg(not(debug_assertions))]
        todo!("Full error/waring display impl is incomplete");
        match self {
            ErrorOrWarning::Error { code, position } => {
                format!("{} {} ({}): {}", l.keywords.compile_words[5], code, position, l.messages.errors[(code.type_code, code.specifier)])
            }
            ErrorOrWarning::Warning { code, position } => {
                format!("{} {} ({}): {}", l.keywords.compile_words[7], code, position, l.messages.warnings[(code.type_code, code.specifier)])
            }
        }
    }
}

impl Display for ErrorCode {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:>02}{:>02}", self.type_code, self.specifier)
    }
}

impl Display for ErrorPosition {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?} {}:{} - {}:{}", self.file, self.pos_start.ln, self.pos_start.col, self.pos_end.ln, self.pos_end.col)
    }
}

impl From<u16> for ErrorCode {
    #[inline]
    fn from(value: u16) -> Self {
        Self {
            type_code: (value >> 8) as u8,
            specifier: (value & 0xff) as u8
        }
    }
}

impl From<usize> for ErrorCode {
    fn from(value: usize) -> Self {
        Self::from(value as u16)
    }
}

impl From<(u8, u8)> for ErrorCode {
    #[inline]
    fn from((v1, v2): (u8, u8)) -> Self {
        Self {
            type_code: v1,
            specifier: v2
        }
    }
}

impl<T: AsRef<Path>> From<(T, (usize, usize))> for ErrorPosition {
    fn from((f, (line, col)): (T, (usize, usize))) -> Self {
        Self {
            file: f.as_ref().to_path_buf(),
            line, col
        }
    }
}
