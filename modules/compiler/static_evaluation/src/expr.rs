use lexer::data_types::NodeWrapper;
use lexer::expr::{Literal, Expr, NumSign};
use lexer::prelude::{Op, Position};
use crate::StaticEval;

impl StaticEval for Expr {
	fn eval(&mut self) {
		// evaluate child nodes first
		match self {
			Expr::Literal(l) => l.node.eval(),
			Expr::ArithOp { left, right, .. } => {
				left.node.eval();
				right.node.eval();
			}
			Expr::Bracket(e) => e.node.eval(),
			Expr::Cast { .. } => {}
		}

		fn eval_arith_op() {

		}

		match self {
			Expr::Literal(_) => {}
			Expr::ArithOp { left, op, right } => {

			}
			Expr::Bracket(_) => {}
			Expr::Cast { .. } => {}
		}
	}
}

#[test]
fn expr() {
	let mut inp = Expr::ArithOp {
		left: Box::new(NodeWrapper {
			node: Expr::Literal(NodeWrapper {
				node: Literal::Int(None, 1u16.into()),
				start: Position { ln: 0, col: 0}, end: Position { ln: 0, col: 1}
			}),
			start: Position { ln: 0, col: 0}, end: Position { ln: 0, col: 1}
		}),
		op: Op::Plus,
		right: Box::new(NodeWrapper {
			node: Expr::Literal(NodeWrapper {
				node: Literal::Int(None, 2u16.into()),
				start: Position { ln: 0, col: 2}, end: Position { ln: 0, col: 3}
			}),
			start: Position { ln: 0, col: 2}, end: Position { ln: 0, col: 3}
		})
	};
	let exp = Expr::Literal(NodeWrapper {
		node: Literal::Int(None, 3u16.into()),
		start: Position { ln: 0, col: 0}, end: Position { ln: 0, col: 3}
	});

	inp.eval();
	assert_eq!(inp, exp);
}

impl StaticEval for Literal {
	fn eval(&mut self) {
		match self {
			Literal::Int(Some(NodeWrapper { node: NumSign::Minus, .. }), val) => *self = Literal::Int(None, -val),
			Literal::Float(Some(NodeWrapper { node: NumSign::Minus, .. }), val) => *self = Literal::Float(None, -val),
			Literal::Bool(Some(_), v) => *self = Literal::Bool(None, !v),
			_ => {}
		}
	}
}
