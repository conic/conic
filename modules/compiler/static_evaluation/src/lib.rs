mod expr;
mod ops;

/// Evaluate `self` statically at compile time
pub trait StaticEval {
	/// Evaluate `self` statically
	fn eval(&mut self);
}
