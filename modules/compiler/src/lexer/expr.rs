use cflp::{Error, NodeWrapper, Parser};
use num_bigint::BigUint;
use lang::tok::{Cmp, Op, Position, Token, TokType};
use crate::lexer::expr::arith_expr::AE0;

mod arith_expr {
	use super::*;
	
	#[derive(Parser, Debug)]
	#[parser(Token, TokType, Position; [[@AE1]], ([TokType::Op(t @ (Op::Plus | Op::Minus)); TokType::Op(Op::Plus)], [[@AE1]])*)]
	pub struct AE0 {
		pub(crate) first: Box<Wrapper<AE1>>,
		pub(crate) rem: Vec<(Op, Box<Wrapper<AE1>>)>
	}
	
	#[derive(Parser, Debug)]
	#[parser(Token, TokType, Position; [[@AE2]], ([TokType::Op(t @ (Op::Div | Op::Mult)); TokType::Op(Op::Mult)], [[@AE2]])*)]
	pub struct AE1 {
		pub(crate) first: Box<Wrapper<AE2>>,
		pub(crate) rem: Vec<(Op, Box<Wrapper<AE2>>)>
	}
	
	#[derive(Parser, Debug)]
	#[parser(Token, TokType, Position; [[@AE3]], (TokType::Op(Op::Pow), [[@AE3]])*)]
	pub struct AE2 {
		pub(crate) first: Box<Wrapper<AE3>>,
		pub(crate) rem: Vec<Box<Wrapper<AE3>>>
	}
	
	#[derive(Parser, Debug)]
	#[parser(Token, TokType, Position; [[@AE4]], (TokType::Op(Op::Mod), [[@AE4]])*)]
	pub struct AE3 {
		pub(crate) first: Box<Wrapper<AE4>>,
		pub(crate) rem: Vec<Box<Wrapper<AE4>>>
	}
	
	#[derive(Parser, Debug)]
	#[parser(Token, TokType, Position)]
	pub enum AE4 {
		#[parser(TokType::LParen, [[@AE0]], TokType::RParen)]
		Bracketed(Box<Wrapper<AE0>>),
		#[parser([[@ExprLiteral]])]
		Lit(Box<Wrapper<ExprLiteral>>)
	}
}

#[derive(Parser, Debug)]
#[parser(Token, TokType, Position)]
pub enum ExprLiteral {
	#[parser([TokType::Int(t)])]
	Int(BigUint),
	#[parser([TokType::Float(t)])]
	Float(f64),
	#[parser([TokType::Bool(t)])]
	Bool(bool),
	#[parser([TokType::String(t)])]
	String(Vec<u8>),
	#[parser([TokType::Char(t)])]
	Char(char)
}

#[cfg(debug_assertions)]
mod debug {
	use std::fmt::{Display, Formatter};
	use lang::tok::{Cmp, Op, TokType};
	use crate::lexer::expr::arith_expr::*;
	use super::ExprLiteral;
	
	impl Display for AE0 {
		fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
			if self.rem.is_empty() { return write!(f, "{}", self.first.node) }
			write!(f, "({})", self.first.node)?;
			for (o, r) in self.rem.iter() {
				write!(f, " {:?} ({})", o, r.node)?;
			}
			Ok(())
		}
	}
	
	impl Display for AE1 {
		fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
			if self.rem.is_empty() { return write!(f, "{}", self.first.node) }
			write!(f, "({})", self.first.node)?;
			for (o, r) in self.rem.iter() {
				write!(f, " {:?} ({})", o, r.node)?;
			}
			Ok(())
		}
	}
	
	impl Display for AE2 {
		fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
			if self.rem.is_empty() { return write!(f, "{}", self.first.node) }
			write!(f, "({})", self.first.node)?;
			for r in self.rem.iter() {
				write!(f, " ** ({})", r.node)?;
			}
			Ok(())
		}
	}
	
	impl Display for AE3 {
		fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
			if self.rem.is_empty() { return write!(f, "{}", self.first.node) }
			write!(f, "({})", self.first.node)?;
			for r in self.rem.iter() {
				write!(f, " % ({})", r.node)?;
			}
			Ok(())
		}
	}
	
	impl Display for AE4 {
		fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
			match self {
				AE4::Bracketed(inner) => write!(f, "[{}]", inner.node),
				AE4::Lit(lit) => write!(f, "{}", lit.node)
			}
		}
	}
	
	impl Display for ExprLiteral {
		fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
			match self {
				ExprLiteral::Int(i) => write!(f, "{}", i),
				ExprLiteral::Float(i) => write!(f, "{}", i),
				ExprLiteral::Bool(i) => write!(f, "{}", i),
				ExprLiteral::String(i) => write!(f, "{}", String::from_utf8_lossy(&*i)),
				ExprLiteral::Char(i) => write!(f, "{}", i),
			}
		}
	}
}

#[cfg(debug_assertions)]
#[test]
fn test_arith() {
	let src = vec![
		Token {
			ps: Position { ln: 0, col: 0 },
			pe: Position { ln: 0, col: 1 },
			tt: TokType::Int(1usize.into()),
		},
		Token {
			ps: Position { ln: 0, col: 2 },
			pe: Position { ln: 0, col: 3 },
			tt: TokType::Op(Op::Plus),
		},
		Token {
			ps: Position { ln: 0, col: 4 },
			pe: Position { ln: 0, col: 5 },
			tt: TokType::Int(2usize.into()),
		},
		Token {
			ps: Position { ln: 0, col: 2 },
			pe: Position { ln: 0, col: 3 },
			tt: TokType::Op(Op::Mult),
		},
		Token {
			ps: Position { ln: 0, col: 4 },
			pe: Position { ln: 0, col: 5 },
			tt: TokType::Int(3usize.into()),
		},
		Token {
			ps: Position { ln: 0, col: 2 },
			pe: Position { ln: 0, col: 3 },
			tt: TokType::Op(Op::Plus),
		},
		Token {
			ps: Position { ln: 0, col: 4 },
			pe: Position { ln: 0, col: 5 },
			tt: TokType::Int(4usize.into()),
		},
	];
	let mut src_iter = src.iter();
	match AE0::parse(&mut src_iter) {
		Ok(ok) => println!("{}", ok.node),
		Err(e) => println!("ERROR: {:?}", e)
	}
	assert!(src_iter.peekable().peek().is_none())
}
