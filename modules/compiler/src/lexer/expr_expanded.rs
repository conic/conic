mod expr {
    use cflp::{Error, NodeWrapper, Parser};
    use num_bigint::BigUint;
    use lang::tok::{Cmp, Op, Position, Token, TokType};
    #[parser(Token, TokType, Position)]
    pub enum Expr {
        #[parser([[@Expr]], [TokType::Op(Op::Plus)|TokType::Op(Op::Minus)], [[@Expr]])]
        Arith0 {
            left: Box<Wrapper<Self>>,
            op: TokType,
            right: Box<Wrapper<Self>>,
        },
        #[parser([[@Expr]], [TokType::Op(Op::Div)|TokType::Op(Op::Mult)], [[@Expr]])]
        Arith1 {
            left: Box<Wrapper<Self>>,
            op: TokType,
            right: Box<Wrapper<Self>>,
        },
        #[parser([[@Expr]], (TokType::Op(Op::Pow), [@Expr])+)]
        Arith2 {
            first: Box<Wrapper<Self>>,
            rem: Vec<Wrapper<Self>>,
        },
        #[parser([[@Expr]], (TokType::Op(Op::Mod), [@Expr])+)]
        Arith3 {
            first: Box<Wrapper<Self>>,
            rem: Vec<Wrapper<Self>>,
        },
        #[parser([[@Expr]], ([TokType::Cmp(t)], [@Expr])+)]
        Compare {
            first: Box<Wrapper<Self>>,
            rem: Vec<(Cmp, Wrapper<Self>)>,
        },
        #[parser(TokType::LParen, [[@Expr]], TokType::RParen)]
        Bracketed(Box<Wrapper<Self>>),
        #[parser([@ExprLiteral])]
        Literal(Wrapper<ExprLiteral>),
    }
    #[automatically_derived]
    impl<'a> cflp::Parser<&'a Token, TokType, cflp::Wrapper<Expr>>
    for Expr {
        fn parse_with_recursion<T: Iterator<Item = &'a Token> + Clone>(
            src: &mut T,
            recurse: bool,
        ) -> Result<cflp::Wrapper<Expr>, cflp::Error<&'a Token, TokType>> {
            use cflp::NodeData;
            let mut start = <Position as Default>::default();
            let mut end = <Position as Default>::default();
            let first_err;
            let src_old = src.clone();
            match 'l0: {
                let left = {
                    if !recurse {
                        break 'l0 Err(cflp::Error {
                            expected: Default::default(),
                            found: None,
                        });
                    }
                    match Expr::parse_with_recursion(src, false) {
                        Ok(t) => {
                            start = t.start();
                            Box::new(t)
                        }
                        Err(e) => break 'l0 Err(e),
                    }
                };
                let op = {
                    let next = src.next();
                    match next.clone().map(Into::<TokType>::into) {
                        Some(
                            next_match @ (TokType::Op(Op::Plus) | TokType::Op(Op::Minus)),
                        ) => next_match,
                        _ => {
                            break 'l0 Err(cflp::Error {
                                found: next,
                                expected: TokType::Op(Op::Plus),
                            });
                        }
                    }
                };
                let right = {
                    match Expr::parse_with_recursion(src, true) {
                        Ok(t) => {
                            end = t.end();
                            Box::new(t)
                        }
                        Err(e) => break 'l0 Err(e),
                    }
                };
                break 'l0 Ok(Expr::Arith0 { left, op, right });
            } {
                Ok(t) => {
                    return Ok(cflp::NodeWrapper {
                        node: t,
                        start,
                        end,
                    });
                }
                Err(e) => {
                    first_err = e;
                    *src = src_old;
                }
            }
            let src_old = src.clone();
            match 'l0: {
                let left = {
                    if !recurse {
                        break 'l0 Err(cflp::Error {
                            expected: Default::default(),
                            found: None,
                        });
                    }
                    match Expr::parse_with_recursion(src, false) {
                        Ok(t) => {
                            start = t.start();
                            Box::new(t)
                        }
                        Err(e) => break 'l0 Err(e),
                    }
                };
                let op = {
                    let next = src.next();
                    match next.clone().map(Into::<TokType>::into) {
                        Some(
                            next_match @ (TokType::Op(Op::Div) | TokType::Op(Op::Mult)),
                        ) => next_match,
                        _ => {
                            break 'l0 Err(cflp::Error {
                                found: next,
                                expected: TokType::Op(Op::Div),
                            });
                        }
                    }
                };
                let right = {
                    match Expr::parse_with_recursion(src, true) {
                        Ok(t) => {
                            end = t.end();
                            Box::new(t)
                        }
                        Err(e) => break 'l0 Err(e),
                    }
                };
                break 'l0 Ok(Expr::Arith1 { left, op, right });
            } {
                Ok(t) => {
                    return Ok(cflp::NodeWrapper {
                        node: t,
                        start,
                        end,
                    });
                }
                Err(_) => *src = src_old,
            }
            let src_old = src.clone();
            match 'l0: {
                let first = {
                    if !recurse {
                        break 'l0 Err(cflp::Error {
                            expected: Default::default(),
                            found: None,
                        });
                    }
                    match Expr::parse_with_recursion(src, false) {
                        Ok(t) => {
                            start = t.start();
                            Box::new(t)
                        }
                        Err(e) => break 'l0 Err(e),
                    }
                };
                let rem = {
                    let mut rem_out = Vec::new();
                    match 'l1: {
                        let next = src.next();
                        if next.clone().map(Into::<TokType>::into)
                            != Some(TokType::Op(Op::Pow))
                        {
                            break 'l1 Err(cflp::Error {
                                expected: TokType::Op(Op::Pow),
                                found: next,
                            });
                        }
                        let rem_out_0 = {
                            match Expr::parse_with_recursion(src, true) {
                                Ok(t) => {
                                    end = t.end();
                                    t
                                }
                                Err(e) => break 'l1 Err(e),
                            }
                        };
                        Ok(rem_out_0)
                    } {
                        Ok(__t) => rem_out.push(__t),
                        Err(e) => break 'l1 Err(e),
                    }
                    loop {
                        let src_old = src.clone();
                        let end_old = end;
                        match 'l1: {
                            let next = src.next();
                            if next.clone().map(Into::<TokType>::into)
                                != Some(TokType::Op(Op::Pow))
                            {
                                break 'l1 Err(cflp::Error {
                                    expected: TokType::Op(Op::Pow),
                                    found: next,
                                });
                            }
                            let rem_out_0 = {
                                match Expr::parse_with_recursion(src, true) {
                                    Ok(t) => {
                                        end = t.end();
                                        t
                                    }
                                    Err(e) => break 'l1 Err(e),
                                }
                            };
                            Ok(rem_out_0)
                        } {
                            Ok(__t) => rem_out.push(__t),
                            _ => {
                                *src = src_old;
                                end = end_old;
                                break;
                            }
                        }
                    }
                    rem_out
                };
                break 'l0 Ok(Expr::Arith2 { first, rem });
            } {
                Ok(t) => {
                    return Ok(cflp::NodeWrapper {
                        node: t,
                        start,
                        end,
                    });
                }
                Err(_) => *src = src_old,
            }
            let src_old = src.clone();
            match 'l0: {
                let first = {
                    if !recurse {
                        break 'l0 Err(cflp::Error {
                            expected: Default::default(),
                            found: None,
                        });
                    }
                    match Expr::parse_with_recursion(src, false) {
                        Ok(t) => {
                            start = t.start();
                            Box::new(t)
                        }
                        Err(e) => break 'l0 Err(e),
                    }
                };
                let rem = {
                    let mut rem_out = Vec::new();
                    match 'l1: {
                        let next = src.next();
                        if next.clone().map(Into::<TokType>::into)
                            != Some(TokType::Op(Op::Mod))
                        {
                            break 'l1 Err(cflp::Error {
                                expected: TokType::Op(Op::Mod),
                                found: next,
                            });
                        }
                        let rem_out_0 = {
                            match Expr::parse_with_recursion(src, true) {
                                Ok(t) => {
                                    end = t.end();
                                    t
                                }
                                Err(e) => break 'l1 Err(e),
                            }
                        };
                        Ok(rem_out_0)
                    } {
                        Ok(__t) => rem_out.push(__t),
                        Err(e) => break 'l1 Err(e),
                    }
                    loop {
                        let src_old = src.clone();
                        let end_old = end;
                        match 'l1: {
                            let next = src.next();
                            if next.clone().map(Into::<TokType>::into)
                                != Some(TokType::Op(Op::Mod))
                            {
                                break 'l1 Err(cflp::Error {
                                    expected: TokType::Op(Op::Mod),
                                    found: next,
                                });
                            }
                            let rem_out_0 = {
                                match Expr::parse_with_recursion(src, true) {
                                    Ok(t) => {
                                        end = t.end();
                                        t
                                    }
                                    Err(e) => break 'l1 Err(e),
                                }
                            };
                            Ok(rem_out_0)
                        } {
                            Ok(__t) => rem_out.push(__t),
                            _ => {
                                *src = src_old;
                                end = end_old;
                                break;
                            }
                        }
                    }
                    rem_out
                };
                break 'l0 Ok(Expr::Arith3 { first, rem });
            } {
                Ok(t) => {
                    return Ok(cflp::NodeWrapper {
                        node: t,
                        start,
                        end,
                    });
                }
                Err(_) => *src = src_old,
            }
            let src_old = src.clone();
            match 'l0: {
                let first = {
                    if !recurse {
                        break 'l0 Err(cflp::Error {
                            expected: Default::default(),
                            found: None,
                        });
                    }
                    match Expr::parse_with_recursion(src, false) {
                        Ok(t) => {
                            start = t.start();
                            Box::new(t)
                        }
                        Err(e) => break 'l0 Err(e),
                    }
                };
                let rem = {
                    let mut rem_out = Vec::new();
                    match 'l1: {
                        let rem_out_0 = {
                            let next = src.next();
                            match next.clone().map(Into::<TokType>::into) {
                                Some(TokType::Cmp(t)) => t.clone(),
                                _ => {
                                    break 'l1 Err(cflp::Error {
                                        found: next,
                                        expected: TokType::Cmp(Default::default()),
                                    });
                                }
                            }
                        };
                        let rem_out_1 = {
                            match Expr::parse_with_recursion(src, true) {
                                Ok(t) => {
                                    end = t.end();
                                    t
                                }
                                Err(e) => break 'l1 Err(e),
                            }
                        };
                        Ok((rem_out_0, rem_out_1))
                    } {
                        Ok(__t) => rem_out.push(__t),
                        Err(e) => break 'l1 Err(e),
                    }
                    loop {
                        let src_old = src.clone();
                        let end_old = end;
                        match 'l1: {
                            let rem_out_0 = {
                                let next = src.next();
                                match next.clone().map(Into::<TokType>::into) {
                                    Some(TokType::Cmp(t)) => t.clone(),
                                    _ => {
                                        break 'l1 Err(cflp::Error {
                                            found: next,
                                            expected: TokType::Cmp(Default::default()),
                                        });
                                    }
                                }
                            };
                            let rem_out_1 = {
                                match Expr::parse_with_recursion(src, true) {
                                    Ok(t) => {
                                        end = t.end();
                                        t
                                    }
                                    Err(e) => break 'l1 Err(e),
                                }
                            };
                            Ok((rem_out_0, rem_out_1))
                        } {
                            Ok(__t) => rem_out.push(__t),
                            _ => {
                                *src = src_old;
                                end = end_old;
                                break;
                            }
                        }
                    }
                    rem_out
                };
                break 'l0 Ok(Expr::Compare { first, rem });
            } {
                Ok(t) => {
                    return Ok(cflp::NodeWrapper {
                        node: t,
                        start,
                        end,
                    });
                }
                Err(_) => *src = src_old,
            }
            let src_old = src.clone();
            match 'l0: {
                let next = src.next();
                if let Some(__next) = next {
                    if Into::<TokType>::into(__next) != TokType::LParen {
                        break 'l0 Err(cflp::Error {
                            expected: TokType::LParen,
                            found: next,
                        });
                    }
                    start = __next.start();
                } else {
                    break 'l0 Err(cflp::Error {
                        expected: TokType::LParen,
                        found: next,
                    })
                };
                let v_0 = {
                    match <Expr as cflp::Parser<
                        _,
                        _,
                        _,
                    >>::parse_with_recursion(src, true) {
                        Ok(t) => Box::new(t),
                        Err(e) => break 'l0 Err(e),
                    }
                };
                let next = src.next();
                if let Some(__next) = next {
                    if Into::<TokType>::into(__next) != TokType::RParen {
                        break 'l0 Err(cflp::Error {
                            expected: TokType::RParen,
                            found: next,
                        });
                    }
                    end = __next.end();
                } else {
                    break 'l0 Err(cflp::Error {
                        expected: TokType::RParen,
                        found: next,
                    })
                };
                break 'l0 Ok(Expr::Bracketed(v_0));
            } {
                Ok(t) => {
                    return Ok(cflp::NodeWrapper {
                        node: t,
                        start,
                        end,
                    });
                }
                Err(_) => *src = src_old,
            }
            let src_old = src.clone();
            match 'l0: {
                let v_0 = {
                    match ExprLiteral::parse_with_recursion(src, true) {
                        Ok(t) => {
                            start = t.start();
                            end = t.end();
                            t
                        }
                        Err(e) => break 'l0 Err(e),
                    }
                };
                break 'l0 Ok(Expr::Literal(v_0));
            } {
                Ok(t) => {
                    return Ok(cflp::NodeWrapper {
                        node: t,
                        start,
                        end,
                    });
                }
                Err(_) => *src = src_old,
            }
            return Err(first_err);
        }
    }
    #[automatically_derived]
    impl ::core::fmt::Debug for Expr {
        fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
            match self {
                Expr::Arith0 { left: __self_0, op: __self_1, right: __self_2 } => {
                    ::core::fmt::Formatter::debug_struct_field3_finish(
                        f,
                        "Arith0",
                        "left",
                        __self_0,
                        "op",
                        __self_1,
                        "right",
                        &__self_2,
                    )
                }
                Expr::Arith1 { left: __self_0, op: __self_1, right: __self_2 } => {
                    ::core::fmt::Formatter::debug_struct_field3_finish(
                        f,
                        "Arith1",
                        "left",
                        __self_0,
                        "op",
                        __self_1,
                        "right",
                        &__self_2,
                    )
                }
                Expr::Arith2 { first: __self_0, rem: __self_1 } => {
                    ::core::fmt::Formatter::debug_struct_field2_finish(
                        f,
                        "Arith2",
                        "first",
                        __self_0,
                        "rem",
                        &__self_1,
                    )
                }
                Expr::Arith3 { first: __self_0, rem: __self_1 } => {
                    ::core::fmt::Formatter::debug_struct_field2_finish(
                        f,
                        "Arith3",
                        "first",
                        __self_0,
                        "rem",
                        &__self_1,
                    )
                }
                Expr::Compare { first: __self_0, rem: __self_1 } => {
                    ::core::fmt::Formatter::debug_struct_field2_finish(
                        f,
                        "Compare",
                        "first",
                        __self_0,
                        "rem",
                        &__self_1,
                    )
                }
                Expr::Bracketed(__self_0) => {
                    ::core::fmt::Formatter::debug_tuple_field1_finish(
                        f,
                        "Bracketed",
                        &__self_0,
                    )
                }
                Expr::Literal(__self_0) => {
                    ::core::fmt::Formatter::debug_tuple_field1_finish(
                        f,
                        "Literal",
                        &__self_0,
                    )
                }
            }
        }
    }
    #[parser(Token, TokType, Position)]
    pub enum ExprLiteral {
        #[parser([TokType::Int(t)])]
        Int(BigUint),
        #[parser([TokType::Float(t)])]
        Float(f64),
        #[parser([TokType::Bool(t)])]
        Bool(bool),
        #[parser([TokType::String(t)])]
        String(Vec<u8>),
        #[parser([TokType::Char(t)])]
        Char(char),
    }
    #[automatically_derived]
    impl<'a> cflp::Parser<&'a Token, TokType, cflp::Wrapper<ExprLiteral>>
    for ExprLiteral {
        fn parse_with_recursion<T: Iterator<Item = &'a Token> + Clone>(
            src: &mut T,
            recurse: bool,
        ) -> Result<
            cflp::Wrapper<ExprLiteral>,
            cflp::Error<&'a Token, TokType>,
        > {
            match src.next() {
                Some(t_unwrapped) => {
                    let start = <Token as cflp::NodeData<Position>>::start(t_unwrapped);
                    let end = <Token as cflp::NodeData<Position>>::end(t_unwrapped);
                    match Into::<TokType>::into(t_unwrapped) {
                        TokType::Int(t) => {
                            Ok(cflp::NodeWrapper {
                                start,
                                end,
                                node: ExprLiteral::Int(t.clone()),
                            })
                        }
                        TokType::Float(t) => {
                            Ok(cflp::NodeWrapper {
                                start,
                                end,
                                node: ExprLiteral::Float(t.clone()),
                            })
                        }
                        TokType::Bool(t) => {
                            Ok(cflp::NodeWrapper {
                                start,
                                end,
                                node: ExprLiteral::Bool(t.clone()),
                            })
                        }
                        TokType::String(t) => {
                            Ok(cflp::NodeWrapper {
                                start,
                                end,
                                node: ExprLiteral::String(t.clone()),
                            })
                        }
                        TokType::Char(t) => {
                            Ok(cflp::NodeWrapper {
                                start,
                                end,
                                node: ExprLiteral::Char(t.clone()),
                            })
                        }
                        _ => {
                            Err(cflp::Error {
                                expected: TokType::Int(Default::default()),
                                found: Some(t_unwrapped),
                            })
                        }
                    }
                }
                _ => {
                    Err(cflp::Error {
                        expected: TokType::Int(Default::default()),
                        found: None,
                    })
                }
            }
        }
    }
    #[automatically_derived]
    impl ::core::fmt::Debug for ExprLiteral {
        fn fmt(&self, f: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
            match self {
                ExprLiteral::Int(__self_0) => {
                    ::core::fmt::Formatter::debug_tuple_field1_finish(
                        f,
                        "Int",
                        &__self_0,
                    )
                }
                ExprLiteral::Float(__self_0) => {
                    ::core::fmt::Formatter::debug_tuple_field1_finish(
                        f,
                        "Float",
                        &__self_0,
                    )
                }
                ExprLiteral::Bool(__self_0) => {
                    ::core::fmt::Formatter::debug_tuple_field1_finish(
                        f,
                        "Bool",
                        &__self_0,
                    )
                }
                ExprLiteral::String(__self_0) => {
                    ::core::fmt::Formatter::debug_tuple_field1_finish(
                        f,
                        "String",
                        &__self_0,
                    )
                }
                ExprLiteral::Char(__self_0) => {
                    ::core::fmt::Formatter::debug_tuple_field1_finish(
                        f,
                        "Char",
                        &__self_0,
                    )
                }
            }
        }
    }
    #[cfg(debug_assertions)]
    mod debug {
        use std::fmt::{Display, Formatter};
        use lang::tok::{Cmp, Op, TokType};
        use super::{Expr, ExprLiteral};
        impl Display for Expr {
            fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
                match self {
                    Expr::Arith0 { left, op, right } => {
                        f.write_fmt(
                            format_args!(
                                "{0} {1} {2}", left.node, if op == & TokType::Op(Op::Plus) {
                                '+' } else { '-' }, right.node
                            ),
                        )
                    }
                    Expr::Arith1 { left, op, right } => {
                        f.write_fmt(
                            format_args!(
                                "{0} {1} {2}", left.node, if op == & TokType::Op(Op::Mult) {
                                '*' } else { '/' }, right.node
                            ),
                        )
                    }
                    Expr::Arith2 { first, rem } => {
                        f.write_fmt(format_args!("{0}", first.node))?;
                        for expr in rem.iter() {
                            f.write_fmt(format_args!(" ** {0}", expr.node))?;
                        }
                        Ok(())
                    }
                    Expr::Arith3 { first, rem } => {
                        f.write_fmt(format_args!("{0}", first.node))?;
                        for expr in rem.iter() {
                            f.write_fmt(format_args!(" % {0}", expr.node))?;
                        }
                        Ok(())
                    }
                    Expr::Compare { first, rem } => {
                        f.write_fmt(format_args!("{0}", first.node))?;
                        for (cmp, expr) in rem.iter() {
                            let cmp = match cmp {
                                Cmp::Eq => "==",
                                Cmp::NE => "!=",
                                Cmp::LT => "<",
                                Cmp::GT => ">",
                                Cmp::LTE => "<=",
                                Cmp::GTE => ">=",
                                Cmp::Any => {
                                    ::core::panicking::panic(
                                        "internal error: entered unreachable code",
                                    )
                                }
                            };
                            f.write_fmt(format_args!(" {0} {1}", cmp, expr.node))?;
                        }
                        Ok(())
                    }
                    Expr::Bracketed(inner) => {
                        f.write_fmt(format_args!("( {0} )", inner.node))
                    }
                    Expr::Literal(lit) => f.write_fmt(format_args!("{0}", lit.node)),
                }
            }
        }
        impl Display for ExprLiteral {
            fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
                match self {
                    ExprLiteral::Int(i) => f.write_fmt(format_args!("{0}", i)),
                    ExprLiteral::Float(i) => f.write_fmt(format_args!("{0}", i)),
                    ExprLiteral::Bool(i) => f.write_fmt(format_args!("{0}", i)),
                    ExprLiteral::String(i) => {
                        f.write_fmt(format_args!("{0}", String::from_utf8_lossy(&* i)))
                    }
                    ExprLiteral::Char(i) => f.write_fmt(format_args!("{0}", i)),
                }
            }
        }
    }
}
