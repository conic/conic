//! Items

use cflp::{Error, NodeWrapper, Parser};
use lang::tok::{Token, TokType, Position};

pub type File = Vec<Item>;

#[derive(Parser)]
#[parser(Token, TokType, Position)]
pub enum Item {
    #[parser([@ItemConst])]
	Const(Wrapper<ItemConst>),
    Enum(Wrapper<ItemEnum>),
    ExternCrate(Wrapper<ItemExternCrate>),
    Fn(Wrapper<ItemFn>),
    ForeignMod(Wrapper<ItemForeignMod>),
    Impl(Wrapper<ItemImpl>),
    Macro(Wrapper<ItemMacro>),
    Mod(Wrapper<ItemMod>),
    Static(Wrapper<ItemStatic>),
    Struct(Wrapper<ItemStruct>),
    Trait(Wrapper<ItemTrait>),
    TraitAlias(Wrapper<ItemTraitAlias>),
    Type(Wrapper<ItemType>),
    Union(Wrapper<ItemUnion>),
    Use(Wrapper<ItemUse>),
}

pub enum Visibility {
    Private,
    Public
}

#[derive(Parser)]
#[parse(Token, TokType, Position; ([@Visibility])?, [TokType::kwd(0)], [@Type], [@Identifier], [TokType::Set(None)])]
pub struct ItemConst {
    vis: Option<Wrapper<Visibility>>,
    const_tok: Token,
    const_type: Wrapper<Type>,
    ident: Wrapper<Identifier>,
    equals: Token,
    expr: Wrapper<Expr>
}
