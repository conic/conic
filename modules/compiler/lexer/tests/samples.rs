use std::fs;
use cflp::Parser;

macro_rules! sample {
    ($name:ident, $file:literal, $expected:expr) => {
        fn $name () -> io::Result<()> {
            let input = fs::read_to_string($file)?;
            let buf = Vec::new();
            let (lang, tables) = lang::get("en").unwrap();
            let toks = lang::tokenize(input.bytes(), lang, &buf, tables).expect("Unable to tokenize input");
            lexer::
        }
    };
}

#[test]
fn test_struct() -> Result<(), String> {
    let input = fs::read_to_string("tests/scripts/struct.fck").map_err(|t| t.to_string())?;
    let buf = Vec::new();
    let (lang, tables) = lang::get("en", &buf).ok_or("Unable to get 'en' language".to_string())?;
    let toks = lang::tokenize(input.bytes(), lang, &buf, tables).expect("Unable to tokenize input");
    let mut toks = toks.iter().peekable();
    let out = lexer::data_types::Struct::parse(&mut toks).map_err(|e| format!("{:?}", e))?;
    println!("{:?}", out);
    Ok(())
}

#[test]
fn test_enum() -> Result<(), String> {
    let input = fs::read_to_string("tests/scripts/enum.fck").map_err(|t| t.to_string())?;
    let buf = Vec::new();
    let (lang, tables) = lang::get("en", &buf).ok_or("Unable to get 'en' language".to_string())?;
    let toks = lang::tokenize(input.bytes(), lang, &buf, tables).expect("Unable to tokenize input");
    let mut toks = toks.iter().peekable();
    let out = lexer::data_types::Enum::parse(&mut toks).map_err(|e| format!("{:?}", e))?;
    println!("{:?}", out);
    Ok(())
}

#[test]
fn test_consts() -> Result<(), String> {
    let input = fs::read_to_string("tests/scripts/consts.fck").map_err(|t| t.to_string())?;
    let buf = Vec::new();
    let (lang, tables) = lang::get("en", &buf).ok_or("Unable to get 'en' language".to_string())?;
    let toks = lang::tokenize(input.bytes(), lang, &buf, tables).expect("Unable to tokenize input");
    let mut toks = toks.iter().peekable();
    for _ in 0..4 {
        let out = lexer::data_types::Const::parse(&mut toks).map_err(|e| format!("{:?}", e))?;
        println!("{:?}", out);
        toks.next(); // skip newline token
    }
    Ok(())
}
