use cflp::{Parser, Scope};
use cflp::NodeWrapper;
use lang::tok::{Token, TokType, Position, PrimitiveKeyword, DataKeyword, Op};
use crate::prelude::{NodeType, Wrapper};

#[cfg_attr(debug_assertions, derive(Debug))]
#[allow(non_camel_case_types)]
#[derive(Parser, Clone)]
#[parser(Token, TokType, NodeType, Position)]
pub enum Type {
    #[parser(TokType::DataKeyword(DataKeyword::KSSelf))]
    TSelf,
    #[parser(TokType::DataKeyword(DataKeyword::KSelf))]
    tself,
    #[parser([TokType::PrimitiveKeyword(t); TokType::PrimitiveKeyword(PrimitiveKeyword::KInt)])]
    Primitive(PrimitiveKeyword),
    #[parser(TokType::Op(Op::Mult), [[@Type]])]
    Pointer(Box<Wrapper<Self>>),
}

impl Scope<NodeType> for Type {
    fn scope() -> NodeType {
        NodeType::Type
    }
}
