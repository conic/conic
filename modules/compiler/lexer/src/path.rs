use cflp::{NodeWrapper, Scope};
use crate::ident::GenericIdent;
use crate::prelude::*;

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Parser)]
#[parser(Token, TokType, NodeType, Position; ([@DoubleColon])?, [@Punctuated<GenericIdent, DoubleColon>])]
pub struct Path {
    optional_prefix: Option<Wrapper<DoubleColon>>,
    pub segments: NodeWrapper<Punctuated<GenericIdent, DoubleColon>, Position>
}

impl Scope<NodeType> for Path {
    fn scope() -> NodeType {
        NodeType::Path
    }
}

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Parser)]
#[parser(Token, TokType, NodeType, Position; TokType::Colon, TokType::Colon)]
pub struct DoubleColon;

impl Scope<NodeType> for DoubleColon {
    fn scope() -> NodeType {
        NodeType::DoubleColon
    }
}
