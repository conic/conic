use crate::{prelude::{NodeType, Wrapper}, types::Type};
use cflp::{Error, Parser, Scope, NodeWrapper};
use num_bigint::BigUint;
use lang::tok::{Token, TokType, Position, Op, NewLine, ControlKeyword};

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Parser, Clone)]
#[parser(Token, TokType, NodeType, Position)]
pub enum Expr {
	#[parser([@Literal])]
	Literal(Wrapper<Literal>),
	#[parser([[@Expr]], [TokType::Op(t)], [[@Expr]])]
	ArithOp {
		left: Box<Wrapper<Self>>,
		op: Op,
		right: Box<Wrapper<Self>>
	},
	#[parser(TokType::LParen, (TokType::NewLine(NewLine::Implicit))*, [[@Expr]], (TokType::NewLine(NewLine::Implicit))*, TokType::RParen)]
	Bracket(Box<Wrapper<Self>>),
	#[parser([[@Expr]], TokType::ControlKeyword(ControlKeyword::KAs), [@Type])]
	Cast {
		expr: Box<Wrapper<Self>>,
		as_ty: Wrapper<Type>
	}
}

impl Scope<NodeType> for Expr {
	fn scope() -> NodeType {
		NodeType::Expr
	}
}

impl Expr {
	pub fn reorder(&mut self, is_reordered: bool) {
		if let Expr::ArithOp { .. } = self {
			if !is_reordered { self.reorder_single() }
		}
		match self {
			Expr::ArithOp { ref mut left, ref mut right, .. } => {
				left.node.reorder(true);
				right.node.reorder(true);
			}
			Expr::Bracket(expr) => expr.node.reorder(false),
			_ => {}
		}
	}

	/// Reorder arithmetic expressions to respect order of operation
	fn reorder_single(&mut self) {
		/*
		this works by getting all the expressions (non-arithmetic) and operator seperators. There will be one more expression that operator
		It then does the following:
		1. Get the index of the most significant operator (`index`)
		2. Remove the values at `index` and `index + 1` (which becomes `index` after removing `index` once) and the operator at `index`
		3. Make an arithop node and place it into `values` at `index`
		4. Repeat until all ops are used, leaving one value in `values`
		5. Pop from `values` and set it to `*self`
		 */
		let mut values = Vec::new();
		let mut ops = Vec::new();
		self.separate_arith_op(&mut values, &mut ops, Default::default(), Default::default());
		if ops.is_empty() { return }
		// The functions on `ops.iter()` comes from `itertools@0.12.0` named `position_max`
		while let Some(index) = ops.iter().enumerate()
			.max_by(|x, y| Ord::cmp(&x.1, &y.1))
			.map(|x| x.0) {
			let left = values.remove(index);
			let right = values.remove(index);
			let out = Box::new(NodeWrapper {
				start: left.start, end: right.end,
				node: Expr::ArithOp {
					left, right,
					op: ops.remove(index),
				},
			});
			values.insert(index, out)
		}
		*self = values.pop().unwrap().node
	}

	fn separate_arith_op(&self, values: &mut Vec<Box<Wrapper<Expr>>>, ops: &mut Vec<Op>, start: Position, end: Position) {
		match self {
			Expr::ArithOp {left, op, right} => {
				left.node.separate_arith_op(values, ops, left.start, left.end);
				ops.push(*op);
				right.node.separate_arith_op(values, ops, right.start, right.end);
			}
			Expr::Literal(l) => values.push(Box::new(NodeWrapper { node: Expr::Literal(l.clone()), start, end })),
			Expr::Bracket(inner) => values.push(Box::new(NodeWrapper { node: Expr::Bracket(inner.clone()), start, end })),
			_ => {}
		}
	}
}

/// Test that `a*b+c*(d/e-f)` is reordered correctly
#[test]
fn test_expr_reorder() {
	let pos = Position { ln: 0, col: 0 };
	let lit = Expr::Literal(NodeWrapper { node: Literal::Char('a'), start: pos, end: pos });
	let mut input = Expr::ArithOp {
		left: Box::new(NodeWrapper { node: lit.clone(), start: pos, end: pos }),
		op: Op::Mult,
		right: Box::new(NodeWrapper {
			node: Expr::ArithOp {
				left: Box::new(NodeWrapper { node: lit.clone(), start: pos, end: pos }),
				op: Op::Plus,
				right: Box::new(NodeWrapper {
					node: Expr::ArithOp {
						left: Box::new(NodeWrapper { node: lit.clone(), start: pos, end: pos }),
						op: Op::Mult,
						right: Box::new(NodeWrapper {
							node: Expr::Bracket(Box::new(NodeWrapper {
								node: Expr::ArithOp {
									left: Box::new(NodeWrapper { node: lit.clone(), start: pos, end: pos }),
									op: Op::Plus,
									right: Box::new(NodeWrapper {
										node: Expr::ArithOp {
											left: Box::new(NodeWrapper { node: lit.clone(), start: pos, end: pos }),
											op: Op::Plus,
											right: Box::new(NodeWrapper { node: lit.clone(), start: pos, end: pos }),
										},
										start: pos, end: pos
									})
								},
								start: pos, end: pos
							})),
							start: pos, end: pos
						})
					},
					start: pos, end: pos
				})
			},
			start: pos, end: pos
		})
	};
	let mut input = Expr::ArithOp {
		left: Box::new(NodeWrapper { node: lit.clone(), start: pos, end: pos }),
		op: Op::Mult,
		right: Box::new(NodeWrapper {
			node: Expr::ArithOp {
				left: Box::new(NodeWrapper { node: lit.clone(), start: pos, end: pos }),
				op: Op::Plus,
				right: Box::new(NodeWrapper {
					node: Expr::ArithOp {
						left: Box::new(NodeWrapper { node: lit.clone(), start: pos, end: pos }),
						op: Op::Mult,
						right: Box::new(NodeWrapper {
							node: Expr::Bracket(Box::new(NodeWrapper {
								node: Expr::ArithOp {
									left: Box::new(NodeWrapper { node: lit.clone(), start: pos, end: pos }),
									op: Op::Div,
									right: Box::new(NodeWrapper {
										node: Expr::ArithOp {
											left: Box::new(NodeWrapper { node: lit.clone(), start: pos, end: pos }),
											op: Op::Minus,
											right: Box::new(NodeWrapper { node: lit.clone(), start: pos, end: pos }),
										},
										start: pos, end: pos
									})
								},
								start: pos, end: pos
							})),
							start: pos, end: pos
						})
					},
					start: pos, end: pos
				})
			},
			start: pos, end: pos
		})
	};
	let expected = Expr::ArithOp {
		left: Box::new(NodeWrapper {
			node: Expr::ArithOp {
				left: Box::new(NodeWrapper { node: lit.clone(), start: pos, end: pos }),
				op: Op::Mult,
				right: Box::new(NodeWrapper { node: lit.clone(), start: pos, end: pos }),
			},
			start: pos, end: pos
		}),
		op: Op::Plus,
		right: Box::new(NodeWrapper {
			node: Expr::ArithOp {
				left: Box::new(NodeWrapper { node: lit.clone(), start: pos, end: pos }),
				op: Op::Mult,
				right: Box::new(NodeWrapper {
					node: Expr::Bracket(Box::new(NodeWrapper {
						node: Expr::ArithOp {
							left: Box::new(NodeWrapper {
								node: Expr::ArithOp {
									left: Box::new(NodeWrapper { node: lit.clone(), start: pos, end: pos }),
									op: Op::Div,
									right: Box::new(NodeWrapper { node: lit.clone(), start: pos, end: pos }),
								},
								start: pos, end: pos
							}),
							op: Op::Minus,
							right: Box::new(NodeWrapper { node: lit.clone(), start: pos, end: pos })
						},
						start: pos, end: pos
					})),
					start: pos, end: pos
				})
			},
			start: pos, end: pos
		})
	};
	input.reorder(false);
	assert_eq!(input, expected)
}

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Parser, Clone)]
#[parser(Token, TokType, NodeType, Position)]
pub enum Literal {
	#[parser(([@NumSign])?, [TokType::Int(t)])]
	Int(Option<Wrapper<NumSign>>, BigUint),
	#[parser(([@NumSign])?, [TokType::Float(t)])]
	Float(Option<Wrapper<NumSign>>, f64),
	#[parser(([TokType::Not])?, [TokType::Bool(t)])]
	Bool(Option<Token>, bool),
	#[parser([TokType::String(t)])]
	String(Vec<u8>),
	#[parser([TokType::Char(t)])]
	Char(char),
}

impl Scope<NodeType> for Literal {
	fn scope() -> NodeType {
		NodeType::Literal
	}
}

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Parser, Copy, Clone)]
#[parser(Token, TokType, NodeType, Position)]
pub enum NumSign {
	#[parser(TokType::Op(Op::Plus))]
	Plus,
	#[parser(TokType::Op(Op::Minus))]
	Minus
}

impl Scope<NodeType> for NumSign {
	fn scope() -> NodeType {
		NodeType::Literal
	}
}
