use std::fmt::{Debug, Formatter};
use std::iter::Peekable;
use std::marker::PhantomData;
pub use cflp::{Parser};
use cflp::{Error, NodeWrapper, Parser as ParserTrait, Scope};
pub use lang::tok::*;
#[cfg(test)]
use crate::{test_node, test::TestDefault};

/// Alias for [`NodeWrapper`](cflp::NodeWrapper) with the positional type filled to save repeating it
pub type Wrapper<T> = NodeWrapper<T, Position>;

pub use seps::*;
mod seps {
	use std::iter::Peekable;
	use super::*;
	#[cfg_attr(debug_assertions, derive(Debug))]
	pub struct CommaSep;

	impl<'a> ParserTrait<&'a Token, TokType, NodeType, Wrapper<CommaSep>> for CommaSep {
		fn parse_with_recursion<T: Iterator<Item=&'a Token> + Clone>(src: &mut Peekable<T>, _: bool) -> Result<Wrapper<CommaSep>, Error<&'a Token, TokType, NodeType>> where Self: Sized {
			match src.next() {
				Some(t) => {
					if t.tt == TokType::Comma {
						Ok(NodeWrapper { start: t.ps, end: t.pe, node: Self })
					} else {
						Err(cflp::Error { expected: TokType::Comma, found: Some(t), scope: vec![NodeType::CommaSep] })
					}
				}
				None => Err(cflp::Error { expected: TokType::Comma, found: None, scope: vec![NodeType::CommaSep] })
			}
		}
	}

	impl Scope<NodeType> for CommaSep {
		fn scope() -> NodeType {
			NodeType::CommaSep
		}
	}

	impl Default for CommaSep {
		fn default() -> Self { Self }
	}

	#[cfg_attr(debug_assertions, derive(Debug))]
	pub struct CommaNewLineSep;

	impl<'a> ParserTrait<&'a Token, TokType, NodeType, Wrapper<CommaNewLineSep>> for CommaNewLineSep {
		fn parse_with_recursion<T: Iterator<Item=&'a Token> + Clone>(src: &mut Peekable<T>, _: bool) -> Result<Wrapper<CommaNewLineSep>, Error<&'a Token, TokType, NodeType>> where Self: Sized {
			loop {
				let src_old = src.clone();
				match src.next() {
					Some(t) => match t.tt {
						TokType::NewLine(NewLine::Implicit) => {},
						_ => { *src = src_old; break }
					}
					_ => return Err(Error {
						found: None,
						expected: TokType::Comma,
						scope: vec![NodeType::CommaSep],
					})
				}
			}
			match src.next() {
				Some(t) if t.tt == TokType::Comma => { }
				t => return Err(cflp::Error { expected: TokType::Comma, found: t, scope: vec![NodeType::CommaSep] })
			}
			loop {
				let src_old = src.clone();
				match src.next() {
					Some(t) => match t.tt {
						TokType::NewLine(NewLine::Implicit) => {},
						_ => { *src = src_old; break }
					}
					_ => { *src = src_old; break }
				}
			}
			Ok(NodeWrapper { node: Self, start: Default::default(), end: Default::default() })
		}
	}

	impl Scope<NodeType> for CommaNewLineSep {
		fn scope() -> NodeType {
			NodeType::CommaSep
		}
	}

	impl Default for CommaNewLineSep {
		fn default() -> Self { Self }
	}

	/// At least one of any type of newline token.\
	///
	/// **DOES NOT PRESERVE POSITIONAL DATA**
	#[cfg_attr(debug_assertions, derive(Debug))]
	pub struct AnyNewLineSep;

	impl<'a> ParserTrait<&'a Token, TokType, NodeType, Wrapper<AnyNewLineSep>> for AnyNewLineSep {
		fn parse_with_recursion<T: Iterator<Item=&'a Token> + Clone>(src: &mut Peekable<T>, _: bool) -> Result<Wrapper<AnyNewLineSep>, Error<&'a Token, TokType, NodeType>> where Self: Sized {
			match src.next() {
				Some(t) => {
					match t.tt {
						TokType::NewLine(_) => {}
						_ => return Err(Error {
							found: Some(t),
							expected: TokType::NewLine(NewLine::Any),
							scope: vec![NodeType::NewLineSep],
						})
					}
				}
				None => return Err(Error { expected: TokType::NewLine(NewLine::Any), found: None, scope: vec![NodeType::NewLineSep] })
			}
			loop {
				let src_old = src.clone();
				match src.next() {
					Some(nxt) => match nxt.tt {
						TokType::NewLine(_) => continue,
						_ => {}
					}
					_ => {}
				}
				*src = src_old;
				return Ok(NodeWrapper { node: Self, start: Default::default(), end: Default::default() })
			}
		}
	}

	impl Scope<NodeType> for AnyNewLineSep {
		fn scope() -> NodeType {
			NodeType::NewLineSep
		}
	}

	impl Default for AnyNewLineSep {
		fn default() -> Self { Self }
	}

	pub fn skip_implicit_newlines<'a, T: Iterator<Item=&'a Token> + Clone>(src: &mut Peekable<T>) {
		loop {
			match src.peek() {
				Some(nxt) => match nxt.tt {
					TokType::NewLine(NewLine::Implicit) => { let _ = src.next(); }
					_ => return
				}
				_ => return
			}
		}
	}
}

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Parser)]
#[parser(Token, TokType, NodeType, Position)]
pub enum Visibility {
	#[parser(TokType::Vis(Vis::Pri))]
	Private,
	#[parser(TokType::Vis(Vis::Pub))]
	Public
}

impl Scope<NodeType> for Visibility {
	fn scope() -> NodeType {
		NodeType::Visibility
	}
}

/// # Punctuated list of values
///
/// This will match a list of values of type `T` separated with values of type `P`. Expects at least one item
///
/// Implements [`IntoIterator`] to iterate over the values captured
pub struct Punctuated<T, P> where T: Sized, P: Sized {
	values: Vec<Wrapper<T>>,
	trailing_punc: PhantomData<P>
}

pub use punctuated_fns::*;
mod punctuated_fns {
	use super::*;

	/// Parse at least one value punctuated with `sep`. Returns the parsed values, starting position, and ending position
	pub fn non_empty_punctuated_toktype<'a, V, T>(src: &mut Peekable<T>, sep: TokType) -> Result<(Vec<Wrapper<V>>, Position, Position), Error<&'a Token, TokType, NodeType>>
		where
			V: ParserTrait<&'a Token, TokType, NodeType, Wrapper<V>> + Sized + Scope<NodeType>,
			T: Iterator<Item=&'a Token> + Clone
	{
		let first = <V as ParserTrait<_, _, NodeType, _>>::parse_with_recursion(src, false)?;
		let start = first.start;
		let mut end = first.end;
		let mut values = vec![first];

		loop {
			if let Some(t) = src.peek() {
				if t.tt != sep { break }
			} else { break }
			let _ = src.next();
			match <V as ParserTrait<_, _, NodeType, _>>::parse_with_recursion(src, false) {
				Ok(val) => {
					end = val.end;
					values.push(val);
				},
				Err(e) => return Err(e)
			}
		}

		Ok((values, start, end))
	}

	pub fn empty_punctuated_toktype<'a, V, T>(src: &mut Peekable<T>, sep: TokType) -> Result<(Vec<Wrapper<V>>, Position, Position), Error<&'a Token, TokType, NodeType>>
		where
			V: ParserTrait<&'a Token, TokType, NodeType, Wrapper<V>> + Sized + Scope<NodeType>,
			T: Iterator<Item=&'a Token> + Clone
	{
		let first = match <V as ParserTrait<_, _, NodeType, Wrapper<V>>>::parse_with_recursion(src, false) {
			Ok(f) => f,
			_ => return Ok((Vec::new(), Default::default(), Default::default()))
		};
		let start = first.start;
		let mut end = first.end;
		let mut values = vec![first];

		loop {
			let src_old = src.clone();
			if let Some(t) = src.next() {
				if t.tt != sep {
					*src = src_old;
					break
				}
			} else {
				*src = src_old;
				break
			}
			let src_old = src.clone();
			match <V as ParserTrait<_, _, NodeType, Wrapper<V>>>::parse_with_recursion(src, false) {
				Ok(val) => {
					end = val.end;
					values.push(val);
				},
				Err(_) => {
					*src = src_old;
					break
				}
			}
		}

		Ok((values, start, end))
	}
	pub fn non_empty_punctuated<'a, V, P, T>(src: &mut Peekable<T>) -> Result<(Vec<Wrapper<V>>, Position, Position), Error<&'a Token, TokType, NodeType>>
		where
			V: ParserTrait<&'a Token, TokType, NodeType, Wrapper<V>> + Sized + Scope<NodeType>,
			P: ParserTrait<&'a Token, TokType, NodeType, Wrapper<P>> + Sized + Scope<NodeType>,
			T: Iterator<Item=&'a Token> + Clone
	{
		let first = <V as ParserTrait<_, _, NodeType, _>>::parse_with_recursion(src, false)?;
		let start = first.start;
		let mut end = first.end;
		let mut values = vec![first];

		loop {
			let src_old = src.clone();
			if <P as ParserTrait<_, _, NodeType, _>>::parse_with_recursion(src, false).is_err() {
				*src = src_old;
				break
			}
			let src_old = src.clone();
			match <V as ParserTrait<_, _, NodeType, _>>::parse_with_recursion(src, false) {
				Ok(val) => {
					end = val.end;
					values.push(val);
				},
				Err(_) => {
					*src = src_old;
					break
				}
			}
		}

		Ok((values, start, end))
	}

	pub fn empty_punctuated<'a, V, P, T>(src: &mut Peekable<T>) -> Result<(Vec<Wrapper<V>>, Position, Position), Error<&'a Token, TokType, NodeType>>
		where
			V: ParserTrait<&'a Token, TokType, NodeType, Wrapper<V>> + Sized + Scope<NodeType>,
			P: ParserTrait<&'a Token, TokType, NodeType, Wrapper<P>> + Sized + Scope<NodeType>,
			T: Iterator<Item=&'a Token> + Clone
	{
		let first = match <V as ParserTrait<_, _, NodeType, Wrapper<V>>>::parse_with_recursion(src, false) {
			Ok(f) => f,
			_ => return Ok((Vec::new(), Default::default(), Default::default()))
		};
		let start = first.start;
		let mut end = first.end;
		let mut values = vec![first];

		loop {
			let src_old = src.clone();
			if <P as ParserTrait<_, _, NodeType, _>>::parse_with_recursion(src, false).is_err() {
				*src = src_old;
				break
			}
			let src_old = src.clone();
			match <V as ParserTrait<_, _, NodeType, Wrapper<V>>>::parse_with_recursion(src, false) {
				Ok(val) => {
					end = val.end;
					values.push(val);
				},
				Err(_) => {
					*src = src_old;
					break
				}
			}
		}

		Ok((values, start, end))
	}
}

impl<T: Sized, P: Sized> From<Vec<Wrapper<T>>> for Punctuated<T, P> {
	fn from(value: Vec<Wrapper<T>>) -> Self {
		Self {
			values: value,
			trailing_punc: PhantomData::default()
		}
	}
}

impl<T, P> Scope<NodeType> for Punctuated<T, P> where T: Scope<NodeType>, P: Scope<NodeType> {
	fn scope() -> NodeType {
		NodeType::Punctuated {
			value: Box::new(<T as Scope<NodeType>>::scope()),
			sep: Box::new(<P as Scope<NodeType>>::scope()),
		}
	}
}

impl<'a, T, P> ParserTrait<&'a Token, TokType, NodeType, Wrapper<Self>> for Punctuated<T, P> where
	T: ParserTrait<&'a Token, TokType, NodeType, Wrapper<T>> + Sized + Scope<NodeType>,
	P: ParserTrait<&'a Token, TokType, NodeType, Wrapper<P>> + Sized + Scope<NodeType>
{
	fn parse_with_recursion<II: Iterator<Item=&'a Token> + Clone>(src: &mut Peekable<II>, _: bool) -> Result<Wrapper<Self>, Error<&'a Token, TokType, NodeType>> where Self: Sized {
		let (start, mut end, mut values) = match <T as ParserTrait<_, _, NodeType, _>>::parse_with_recursion(src, false) {
			Ok(val) => (val.start, val.end, vec![val]),
			Err(mut e) => {
				e.push_scope(<Self as Scope<NodeType>>::scope());
				return Err(e)
			}
		};

		loop {
			let src_old = src.clone();
			if <P as ParserTrait<_, _, NodeType, _>>::parse_with_recursion(src, false).is_err() {
				*src = src_old;
				break
			}
			let src_old = src.clone();
			match <T as ParserTrait<_, _, NodeType, _>>::parse_with_recursion(src, false) {
				Ok(val) => {
					end = val.end;
					values.push(val);
				},
				Err(_) => { *src = src_old; break }
			}
		}
		
		Ok(NodeWrapper {
			start, end, node: Self { values, trailing_punc: PhantomData::default() }
		})
	}
}

impl<T, P> IntoIterator for Punctuated<T, P> where T: Sized, P: Sized {
	type Item = Wrapper<T>;
	type IntoIter = <Vec<Self::Item> as IntoIterator>::IntoIter;
	
	fn into_iter(self) -> Self::IntoIter {
		self.values.into_iter()
	}
}

#[cfg(debug_assertions)]
impl<T, P> Debug for Punctuated<T, P> where T: Sized + Debug, P: Sized {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		f.debug_struct("Punctuated")
			.field("values", &self.values)
			.finish()
	}
}

#[derive(Debug)]
pub enum NodeType {
	/// Values separated by punctuation
	Punctuated {
		/// Value matched
		value: Box<Self>,
		/// Separator scope
		sep: Box<Self>
	},
	/// Comma separator
	CommaSep,
	/// Newline separator
	NewLineSep,
	/// Plus (`+`) separator for generic constraints
	PlusSep,
	/// Double colon separator (`::`)
	DoubleColon,
	/// Type name
	Type,
	/// Generic arguments inside angled brackets `<` and `>`
	GenericArguments,
	/// Singular generic argument
	GenericArgument,
	/// `where` statement
	Where,
	/// Constrain on a generic
	GenericConstraint,
	/// Identifier
	Ident,
	/// Identifier with optional generics
	GenericIdent,
	/// Struct type definition
	StructDef,
	/// Properties section of a [struct def](NodeType::StructDef)
	StructProperties,
	/// Struct property under [struct properties](NodeType::StructProperties)
	StructProperty,
	/// Struct type definition
	EnumDef,
	/// Variants section of an [enum def](NodeType::EnumDef)
	EnumVariants,
	/// Struct property under [struct properties](NodeType::EnumVariants)
	EnumVariant,
	/// type id pair in a struct-type enum variant field
	EnumStructTypeField,
	/// Path. `GenericIdent` with `DoubleColon` separators
	Path,
	/// Visibility identifier (`pub` or `pri`)
	Visibility,
	/// Function arguments
	FnArgs,
	/// Function argument such as `*self` or `int someVar`
	FnArg,
	/// Function
	Function,
	/// Constant definition
	ConstDef,
	/// Expression
	Expr,
	/// Literal value
	Literal
}
