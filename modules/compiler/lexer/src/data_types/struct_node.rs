use super::{prelude::*, function::SelfFunction};

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Parser)]
#[parser(Token, TokType, NodeType, Position;
([@Visibility])?, TokType::DataKeyword(DataKeyword::KStruct), [@Ident], ([@GenericArguments])?,
(TokType::NewLine(NewLine::Implicit))*, ([@Where])?, (TokType::NewLine(NewLine::Implicit))*,
TokType::LParenCurly, (TokType::NewLine(NewLine::Implicit))*,
([@SelfFunction], (TokType::NewLine(NewLine::Implicit))*)*,
[@StructProperties], (TokType::NewLine(NewLine::Implicit))*,
([@SelfFunction], (TokType::NewLine(NewLine::Implicit))*)*,
TokType::RParenCurly
)]
pub struct Struct {
	pub vis: Option<Wrapper<Visibility>>,
	pub ident: Wrapper<Ident>,
	pub generics: Option<Wrapper<GenericArguments>>,
	pub where_stmt: Option<Wrapper<Where>>,
	pre_prop_fns: Vec<Wrapper<SelfFunction>>,
	pub properties: Wrapper<StructProperties>,
	post_prop_fns: Vec<Wrapper<SelfFunction>>
}


impl Scope<NodeType> for Struct {
	fn scope() -> NodeType {
		NodeType::StructDef
	}
}

impl Struct {
	/// Get all the functions defined for this struct. Only includes functions defined inside the struct not
	/// any functions added by extension
	pub fn functions(&self) -> Vec<&Wrapper<SelfFunction>> {
		self.pre_prop_fns.iter().chain(self.post_prop_fns.iter()).collect()
	}
}

/// The `properties` section of a `struct` data type
#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Parser)]
#[parser(Token, TokType, NodeType, Position;
TokType::DataKeyword(DataKeyword::KProperties), (TokType::NewLine(_))*, TokType::LParenCurly, (TokType::NewLine(_))*,
([@StructProperty], (TokType::NewLine(_))+)*,
TokType::RParenCurly
)]
pub struct StructProperties {
	pub properties: Vec<Wrapper<StructProperty>>
}

impl Scope<NodeType> for StructProperties {
	fn scope() -> NodeType {
		NodeType::StructProperties
	}
}

/// A singular property for a struct; the `int id` or `str name` in
/// ```conic
/// !!en
/// struct Example {
///     properties {
///         int id,
///         str name
/// 	}
/// }
/// ```
#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Parser)]
#[parser(Token, TokType, NodeType, Position; ([@Visibility])?, [@Type], [@Ident])]
pub struct StructProperty {
	pub vis: Option<Wrapper<Visibility>>,
	pub ty: Wrapper<Type>,
	pub ident: Wrapper<Ident>,
}

impl Scope<NodeType> for StructProperty {
	fn scope() -> NodeType {
		NodeType::StructProperty
	}
}
