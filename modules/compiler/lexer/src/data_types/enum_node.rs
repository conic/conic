use std::iter::Peekable;
use cflp::Error;
use super::{prelude::*, function::SelfFunction};

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Parser)]
#[parser(Token, TokType, NodeType, Position;
([@Visibility])?, TokType::DataKeyword(DataKeyword::KEnum), [@Ident], ([@GenericArguments])?,
(TokType::NewLine(NewLine::Implicit))*, ([@Where])?, (TokType::NewLine(NewLine::Implicit))*,
TokType::LParenCurly, (TokType::NewLine(NewLine::Implicit))*,
([@SelfFunction], (TokType::NewLine(NewLine::Implicit))*)*,
[@EnumVariants], (TokType::NewLine(NewLine::Implicit))*,
([@SelfFunction], (TokType::NewLine(NewLine::Implicit))*)*,
TokType::RParenCurly
)]
pub struct Enum {
	pub vis: Option<Wrapper<Visibility>>,
	pub ident: Wrapper<Ident>,
	pub generics: Option<Wrapper<GenericArguments>>,
	pub where_stmt: Option<Wrapper<Where>>,
	pre_prop_fns: Vec<Wrapper<SelfFunction>>,
	pub variants: Wrapper<EnumVariants>,
	post_prop_fns: Vec<Wrapper<SelfFunction>>
}


impl Scope<NodeType> for Enum {
	fn scope() -> NodeType {
		NodeType::EnumDef
	}
}

impl Enum {
	/// Get all the functions defined for this struct. Only includes functions defined inside the struct not
	/// any functions added by extension
	pub fn functions(&self) -> Vec<&Wrapper<SelfFunction>> {
		self.pre_prop_fns.iter().chain(self.post_prop_fns.iter()).collect()
	}
}

/// The `properties` section of a `struct` data type
#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Parser)]
#[parser(Token, TokType, NodeType, Position;
TokType::DataKeyword(DataKeyword::KVariants), (TokType::NewLine(_))*, TokType::LParenCurly, (TokType::NewLine(_))*,
([@EnumVariant], (TokType::NewLine(_))+)*,
TokType::RParenCurly
)]
pub struct EnumVariants {
	pub variants: Vec<Wrapper<EnumVariant>>
}

impl Scope<NodeType> for EnumVariants {
	fn scope() -> NodeType {
		NodeType::EnumVariants
	}
}

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Parser)]
#[parser(Token, TokType, NodeType, Position; [@Ident], [@EnumVariantData])]
pub struct EnumVariant {
	pub id: Wrapper<Ident>,
	pub kind: Wrapper<EnumVariantData>
}

impl Scope<NodeType> for EnumVariant {
	fn scope() -> NodeType {
		NodeType::EnumVariant
	}
}

#[cfg_attr(debug_assertions, derive(Debug))]
pub enum EnumVariantData {
	TupleType(Vec<Wrapper<Type>>),
	StructType(Vec<Wrapper<EnumStructTypeField>>),
	Empty
}

impl Scope<NodeType> for EnumVariantData {
	fn scope() -> NodeType {
		NodeType::EnumVariant
	}
}

impl<'a> Parser<&'a Token, TokType, NodeType, Wrapper<Self>> for EnumVariantData {
	fn parse_with_recursion<T: Iterator<Item=&'a Token> + Clone>(src: &mut Peekable<T>, _: bool) -> Result<Wrapper<Self>, Error<&'a Token, TokType, NodeType>> where Self: Sized {
		let src_old = src.clone();
		let nxt = if let Some(nxt) = src.next() {
			nxt
		} else {
			return Ok(NodeWrapper {
				node: Self::Empty,
				start: Default::default(), end: Default::default()
			})
		};
		match nxt.tt {
			TokType::LParen => {
				skip_implicit_newlines(src);
				let (values, _, _) = empty_punctuated::<Type, CommaNewLineSep, T>(src).map_err(|mut e| e.push_scope_self(<Self as Scope<NodeType>>::scope()))?;
				skip_implicit_newlines(src);
				match src.next() {
					Some(t) if t.tt == TokType::RParen => {
						Ok(NodeWrapper {
							node: Self::TupleType(values),
							start: nxt.ps,
							end: t.pe,
						})
					}
					other => Err(Error {
						found: other,
						expected: TokType::RParen,
						scope: vec![<Self as Scope<NodeType>>::scope()],
					})
				}
			}
			TokType::LParenCurly => {
				skip_implicit_newlines(src);
				let (values, _, _) = empty_punctuated::<EnumStructTypeField, CommaNewLineSep, T>(src).map_err(|mut e| e.push_scope_self(<Self as Scope<NodeType>>::scope()))?;
				skip_implicit_newlines(src);
				match src.next() {
					Some(t) if t.tt == TokType::RParenCurly => {
						Ok(NodeWrapper {
							node: Self::StructType(values),
							start: nxt.ps,
							end: t.pe,
						})
					}
					other => Err(Error {
						found: other,
						expected: TokType::RParenCurly,
						scope: vec![<Self as Scope<NodeType>>::scope()],
					})
				}
			}
			TokType::NewLine(_) => {
				*src = src_old;
				Ok(NodeWrapper {
					node: Self::Empty,
					start: Default::default(), end: Default::default()
				})
			},
			_ => Err(Error {
				found: Some(nxt),
				expected: TokType::LParenCurly,
				scope: vec![<Self as Scope<NodeType>>::scope()],
			})
		}
	}
}

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Parser)]
#[parser(Token, TokType, NodeType, Position; [@Type], [@Ident])]
pub struct EnumStructTypeField {
	pub ty: Wrapper<Type>,
	pub id: Wrapper<Ident>
}

impl Scope<NodeType> for EnumStructTypeField {
	fn scope() -> NodeType {
		NodeType::EnumStructTypeField
	}
}