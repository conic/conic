use std::iter::Peekable;
use cflp::Error;
use super::prelude::*;

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Parser)]
#[parser(Token, TokType, NodeType, Position;
([@Visibility])?, TokType::ControlKeyword(ControlKeyword::KFn), [@Ident], ([@GenericArguments])?,
(TokType::NewLine(NewLine::Implicit))*, TokType::LParen, (TokType::NewLine(NewLine::Implicit))*, [@FnArgs<T>], (TokType::NewLine(NewLine::Implicit))*, TokType::RParen,
TokType::LParenCurly, TokType::RParenCurly
)]
#[parser_constraint(T)]
pub struct Function<T> {
	pub vis: Option<Wrapper<Visibility>>,
	pub ident: Wrapper<Ident>,
	pub generics: Option<Wrapper<GenericArguments>>,
	pub args: Wrapper<FnArgs<T>>,
	// pub where_stmt: Option<Wrapper<Where>>,
}

pub type RawFunction = Function<FnArg>;
pub type SelfFunction = Function<SelfFnArg>;

impl<T> Scope<NodeType> for Function<T> {
	fn scope() -> NodeType { NodeType::Function }
}

/// Function arguments
///
/// This contains a generic to parse the first argument so that this can optionally be [`SelfFnArg`] if inside a struct
/// or enum definition or extension, but can also be a regular [`FnArg`] for other functions.
///
/// Expects at least zero arguments
#[cfg_attr(debug_assertions, derive(Debug))]
#[cfg_attr(test, derive(PartialEq, Clone))]
pub struct FnArgs<T> {
	pub first_arg: Option<Wrapper<T>>,
	pub remaining_args: Vec<Wrapper<FnArg>>
}

impl<'a, F> Parser<&'a Token, TokType, NodeType, Wrapper<Self>> for FnArgs<F> where
	F: Parser<&'a Token, TokType, NodeType, Wrapper<F>>
{
	fn parse_with_recursion<T: Iterator<Item=&'a Token> + Clone>(src: &mut Peekable<T>, _recurse: bool) -> Result<Wrapper<Self>, Error<&'a Token, TokType, NodeType>> where Self: Sized {
		let (start, mut end, first_arg) = if let Ok(fa) = F::parse(src) {
			(fa.start, fa.end, Some(fa))
		} else {
			return Ok(NodeWrapper {
				node: Self { first_arg: None, remaining_args: Vec::new() },
				start: Default::default(),
				end: Default::default()
			})
		};

		let mut remaining_args = Vec::new();

		loop {
			loop {
				let src_old = src.clone();
				if let Some(nxt) = src.next() {
					if nxt != TokType::NewLine(NewLine::Implicit) {
						*src = src_old;
						break
					}
				}
			}
			if let Some(nxt) = src.peek() {
				if nxt.tt != TokType::Comma {
					break
				} else { let _ = src.next(); }
			} else {
				break
			}
			loop {
				if let Some(nxt) = src.peek() {
					if nxt.tt != TokType::NewLine(NewLine::Implicit) {
						break
					} else { let _ = src.next(); }
				}
			}
			match <FnArg as Parser<_, _, NodeType, _>>::parse_with_recursion(src, false) {
				Ok(val) => {
					end = val.end;
					remaining_args.push(val);
				},
				Err(mut e) => {
					e.push_scope(<Self as Scope<NodeType>>::scope());
					return Err(e)
				}
			}
		}

		Ok(NodeWrapper {
			node: Self { first_arg, remaining_args },
			start, end
		})
	}
}

impl<T> Scope<NodeType> for FnArgs<T> {
	fn scope() -> NodeType { NodeType::FnArgs }
}

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Parser)]
#[parser(Token, TokType, NodeType, Position; [@Type], [@Ident])]
pub struct FnArg {
	pub ty: Wrapper<Type>,
	pub ident: Wrapper<Ident>
}

impl Scope<NodeType> for FnArg {
	fn scope() -> NodeType { NodeType::FnArg }
}

#[cfg_attr(debug_assertions, derive(Debug))]
pub struct SelfFnArg {
	pub pointer: bool
}

impl<'a> Parser<&'a Token, TokType, NodeType, Wrapper<Self>> for SelfFnArg {
	fn parse_with_recursion<T: Iterator<Item=&'a Token> + Clone>(src: &mut Peekable<T>, _recurse: bool) -> Result<Wrapper<Self>, Error<&'a Token, TokType, NodeType>> where Self: Sized {
		if let Some(nxt) = src.next() {
			if nxt != TokType::Op(Op::Mult) {
				if nxt != TokType::DataKeyword(DataKeyword::KSelf) {
					Err(Error {
						expected: TokType::DataKeyword(DataKeyword::KSelf),
						found: None,
						scope: vec![<Self as Scope<NodeType>>::scope()]
					})
				} else {
					Ok(NodeWrapper {
						node: Self { pointer: false },
						start: nxt.ps,
						end: nxt.pe,
					})
				}
			} else {
				if let Some(nxt_s) = src.next() {
					if nxt_s != TokType::DataKeyword(DataKeyword::KSelf) {
						Err(Error {
							expected: TokType::DataKeyword(DataKeyword::KSelf),
							found: None,
							scope: vec![<Self as Scope<NodeType>>::scope()]
						})
					} else {
						Ok(NodeWrapper {
							node: Self { pointer: true },
							start: nxt.ps,
							end: nxt_s.pe,
						})
					}
				} else {
					Err(Error {
						expected: TokType::DataKeyword(DataKeyword::KSelf),
						found: None,
						scope: vec![<Self as Scope<NodeType>>::scope()]
					})
				}
			}
		} else {
			Err(Error {
				expected: TokType::DataKeyword(DataKeyword::KSelf),
				found: None,
				scope: vec![<Self as Scope<NodeType>>::scope()]
			})
		}
	}
}

impl Scope<NodeType> for SelfFnArg {
	fn scope() -> NodeType { NodeType::FnArg }
}