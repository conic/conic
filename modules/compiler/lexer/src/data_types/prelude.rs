use std::iter::Peekable;
pub use cflp::{NodeWrapper, Scope};
use cflp::Error;
pub use crate::ident::{GenericArguments, GenericIdent, Ident};
pub use crate::prelude::*;
#[cfg(test)]
pub use crate::{test::TestDefault, test_node, token, position};
pub use crate::types::Type;

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Parser)]
#[parser(Token, TokType, NodeType, Position; [TokType::ControlKeyword(ControlKeyword::KWhere)], [@Punctuated<GenericConstraint, CommaSep>])]
pub struct Where {
	pub where_tok: Token,
	pub constraints: NodeWrapper<Punctuated<GenericConstraint, CommaSep>, Position>
}

impl Scope<NodeType> for Where {
	fn scope() -> NodeType { NodeType::Where }
}

#[cfg_attr(debug_assertions, derive(Debug))]
pub struct GenericConstraint {
	pub generic: Wrapper<Ident>,
	pub constraints: Vec<NodeWrapper<GenericIdent, Position>>,
}

impl Scope<NodeType> for GenericConstraint {
	fn scope() -> NodeType { NodeType::GenericConstraint }
}

impl<'a> Parser<&'a Token, TokType, NodeType, Wrapper<Self>> for GenericConstraint {
	fn parse_with_recursion<T: Iterator<Item=&'a Token> + Clone>(src: &mut Peekable<T>, _: bool) -> Result<Wrapper<Self>, Error<&'a Token, TokType, NodeType>> where Self: Sized {
		let generic = <Ident as Parser<_, _, NodeType, _>>::parse_with_recursion(src, false).map_err(|mut e| e.push_scope_self(<Self as Scope<NodeType>>::scope()))?;
		match src.next() {
			Some(t) if t.tt == TokType::Colon => {}
			other => return Err(Error {
				found: other,
				expected: TokType::Colon,
				scope: vec![<Self as Scope<NodeType>>::scope()],
			})
		}
		let (constraints, _, end) = non_empty_punctuated_toktype::<GenericIdent, T>(src, TokType::Op(Op::Plus)).map_err(|mut e| e.push_scope_self(<Self as Scope<NodeType>>::scope()))?;
		Ok(NodeWrapper {
			start: generic.start, end,
			node: Self { generic, constraints },
		})
	}
}
