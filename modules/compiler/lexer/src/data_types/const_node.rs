use crate::expr::Expr;
use super::prelude::*;

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Parser)]
#[parser(Token, TokType, NodeType, Position;
	([@Visibility])?, TokType::DataKeyword(DataKeyword::KConst), [@Type], [@Ident],
	TokType::Set(None), [@Expr]
)]
pub struct Const {
	pub vis: Option<Wrapper<Visibility>>,
	pub ty: Wrapper<Type>,
	pub id: Wrapper<Ident>,
	pub val: Wrapper<Expr>
}

impl Scope<NodeType> for Const {
	fn scope() -> NodeType {
		NodeType::ConstDef
	}
}
