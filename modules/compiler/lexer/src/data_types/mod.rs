macro_rules! mods {
    ($($m:ident),+$(,)?) => {$(mod $m; pub use $m::*;)+};
}

mods!{struct_node, enum_node, function, prelude, const_node}
