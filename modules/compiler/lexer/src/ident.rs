use cflp::{NodeWrapper, Scope};
use crate::prelude::*;
use crate::types::Type;

/// Identifier
///
/// Contains a language and id
#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Parser)]
#[parser(Token, TokType, NodeType, Position; [TokType::Identifier(lang, id)])]
pub struct Ident(pub (String, Vec<u8>));

impl Scope<NodeType> for Ident {
	fn scope() -> NodeType { NodeType::Ident }
}

/// [Identifier](Ident) with optional [generic arguments](GenericArguments)
#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Parser)]
#[parser(Token, TokType, NodeType, Position; [@Ident], ([@GenericArguments])?)]
pub struct GenericIdent {
	ident: Wrapper<Ident>,
	generics: Option<Wrapper<GenericArguments>>
}

impl Scope<NodeType> for GenericIdent {
	fn scope() -> NodeType { NodeType::GenericIdent }
}

#[cfg_attr(debug_assertions, derive(Debug))]
#[cfg_attr(test, derive(PartialEq, Clone))]
#[derive(Parser)]
#[parser(Token, TokType, NodeType, Position; TokType::Cmp(Cmp::LT), [@Punctuated<GenericArgument, CommaSep>], TokType::Cmp(Cmp::GT))]
/// 1. `TokType::Cmp(Cmp::LT)`
/// 2. [`[@Punctuated<GenericArgument, CommaSep>]`](GenericArgument)
/// 3. `TokType::Cmp(Cmp::GT)`
pub struct GenericArguments (pub NodeWrapper<Punctuated<GenericArgument, CommaSep>, Position>);

impl Scope<NodeType> for GenericArguments {
	fn scope() -> NodeType { NodeType::GenericArguments }
}

#[cfg(test)]
impl TestDefault for GenericArguments {
	fn defaults() -> Vec<(Vec<Token>, Wrapper<Self>)> {
		let mut out = Vec::new();

		let generic_all = <GenericArgument as TestDefault>::defaults();
		// one generic (no commas)
		// three generics (commas)
		for i in [1, 3] {
			let chunked_perms = &generic_all.iter().permutations(i).chunks(20);
			for perm in chunked_perms.into_iter().next().unwrap() {
				let punc = Punctuated::from(perm.iter().map(|(_, t)| t.clone()).collect::<Vec<_>>());
				let mut toks = vec![Token {
					tt: TokType::Cmp(Cmp::LT),
					ps: Position { ln: 5, col: 2},
					pe: Position { ln: 7, col: 3}
				}];
				let (_, first_node) = perm.first().unwrap();
				let ((end, end_node), pre) = perm.split_last().unwrap();
				for (input, _) in pre.iter() {
					toks.extend(input.iter().map(|t| t.clone()));
					toks.push(Token {
						tt: TokType::Comma,
						ps: Position { ln: 0, col: 0 },
						pe: Position { ln: 0, col: 0 }
					});
				}
				toks.extend(end.iter().map(|t| t.clone()));
				toks.push(Token {
					tt: TokType::Cmp(Cmp::GT),
					ps: Position { ln: 12, col: 0},
					pe: Position { ln: 12, col: 6}
				});
				out.push((toks, NodeWrapper {
					node: Self(NodeWrapper {
						node: punc,
						start: first_node.start,
						end: end_node.end,
					}),
					start: Position { ln: 5, col: 2},
					end: Position { ln: 12, col: 6},
				}))
			}
		}

		out
	}
}

#[cfg(test)]
test_node!(GenericArguments);

/// Generic argument. Either a [type](GenericArgument::Const) or [identifier](GenericArgument::Ident)
#[cfg_attr(debug_assertions, derive(Debug))]
#[cfg_attr(test, derive(PartialEq, Clone))]
#[derive(Parser)]
#[parser(Token, TokType, NodeType, Position)]
pub enum GenericArgument {
	/// Constant type generic argument (`const <type> <ident>`)
	#[parser(TokType::DataKeyword(DataKeyword::KConst), [@Type], [@Ident])]
	Const {
		const_ty: Wrapper<Type>,
		ident: Wrapper<Ident>
	},
	/// Identifier generic
	#[parser([@Ident])]
	Ident(Wrapper<Ident>)
}

impl Scope<NodeType> for GenericArgument {
	fn scope() -> NodeType { NodeType::GenericArgument }
}

#[cfg(test)]
impl TestDefault for GenericArgument {
	fn defaults() -> Vec<(Vec<Token>, Wrapper<Self>)> {
		let idents = <Ident as TestDefault>::defaults();
		let types = <Type as TestDefault>::defaults();

		let mut out = idents.iter()
			.map(|(tks, out)| (tks.clone(), NodeWrapper { start: out.start, end: out.end, node: Self::Ident(out.clone()) }))
			.collect::<Vec<_>>();

		for ty in types.iter() {
			for id in idents.iter() {
				let mut toks = vec![token!(TokType::DataKeyword(DataKeyword::KConst), (0, 0), (1, 1))];
				toks.extend(ty.0.iter().map(Clone::clone));
				toks.extend(id.0.iter().map(Clone::clone));
				out.push((toks, NodeWrapper {
					node: Self::Const { const_ty: ty.1.clone(), ident: id.1.clone() },
					start: position!(0, 0),
					end: id.1.end,
				}));
			}
		}

		out
	}
}

#[cfg(test)]
test_node!(GenericArgument);
