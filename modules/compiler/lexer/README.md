# Lexer module

This crate contains all the structs and enums for parsing a token stream into an AST.

## Testing

Testing is done both inside the crate and inside an external test suite:
1. Internal testing consists of individual tests performed on inputs returned from a trait:
    ```rust
    trait TestDefault where Self: PartialEq {
        fn defaults() -> Vec<(Vec<Token>, Wrapper<Self>)>;
    }
    ```
   The tests are then generated through a macro and can optionally be modified to have a maximum number of tests done per type with the `TEST_LIMIT` environment variable (this must be an integer)
2. External testing uses sample files and both tokenizes and lexes the file and compares it against an expected node tree
