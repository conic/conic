use std::collections::{HashMap, HashSet};
use std::hash::{Hash, Hasher};
use std::path::PathBuf;
use super::semver::{SemVer, SemVerValue};

pub struct Manifest {
	pub(crate) package: Package,
	pub(crate) dependencies: Dependencies,
	pub(crate) language: String,
}

/// # Package section of the manifest file
///
/// Contains all the values from the package section of a manifest file
pub struct Package {
	/// Package name
	pub name: String,
	/// Path to the source code
	pub src: PathBuf,
	/// Path to the tests folder
	pub tests: Option<PathBuf>,
	/// Path to the benches folder
	pub benches: Option<PathBuf>,
	/// Kind of package. See [`PackageType`]
	pub kind: PackageType,
	/// Package version
	pub version: SemVerValue,
	/// Authors. Map keys are author names, with values containing author data
	pub authors: HashMap<String, Author>,
	/// Package license. See [`License`]
	pub license: Option<License>,
	/// Description of the package
	pub description: Option<String>,
	/// README file
	pub readme: Option<String>,
	/// Homepage of the project
	pub homepage: Option<String>,
	/// Repository URL
	pub repo: Option<String>,
	/// Features. Map keys are feature names, and values are optional feature descriptions
	pub features: HashMap<String, Option<String>>
}

/// # Package type
///
/// Specifies if the package is a library, application, or both
pub enum PackageType {
	/// Library package
	Lib { #[doc("Path to library entrypoint")]path: PathBuf },
	/// Application package
	App { #[doc("Path to application entrypoint")]path: PathBuf },
	/// Library and application package
	LibApp {
		/// Path to library entrypoint
		lib_path: PathBuf,
		/// Path to application entrypoint
		app_path: PathBuf
	},
}

impl PackageType {
	pub(crate) fn extend(&mut self, other: PathBuf) {
		*self = match self {
			PackageType::Lib { path } => Self::LibApp {
				lib_path: path.clone(),
				app_path: other
			},
			PackageType::App { path } => Self::LibApp {
				app_path: path.clone(),
				lib_path: other
			},
			PackageType::LibApp { .. } => unreachable!("Should not be reached due to line checks")
		}
	}
}

/// # Author value
///
/// Contains values available to attach to an author
pub struct Author {
	/// Author email
	pub email: Option<String>,
	/// GitHub username
	pub github: Option<String>,
	/// GitLab username
	pub gitlab: Option<String>
}

/// # License type
///
/// Types of license available
pub enum License {
	/// License name from [SPDX v3.21](https://github.com/spdx/license-list-data/tree/v3.21)
	Named {
		id: String,
		name: String
	},
	/// Path to a license file (takes precedence)
	File(PathBuf)
}

/// # Dependencies
///
/// Contains the universal dependencies, build and development dependencies, and target specific
/// dependencies
pub struct Dependencies {
	/// Universal dependencies
	pub(crate) dependencies: DependenciesInner,
	/// Target specific dependencies
	pub(crate) targets: HashMap<String, DependenciesInner>
}

/// # Dependency structure
///
/// This is the container for all dependencies. Dependencies are stored in a [`HashSet`] and
/// [`Dependency::eq`] checks for name equality, so no two dependencies can have the same name,
/// regardless of version.
///
/// This contains [base dependencies](DependenciesInner::base) used for everything;
/// [dev dependencies](DependenciesInner::dev) used for non-release builds or interpreted code; and
/// [build dependencies](DependenciesInner::base) used for building the project
pub struct DependenciesInner {
	/// Base dependencies. These are used for everything the dependencies are scoped for
	pub base: HashSet<Dependency>,
	/// Development dependencies. These are included unless building the code for release
	pub dev: HashSet<Dependency>,
	/// Build dependencies. These are used only for build scripts and are not available anywhere
	/// else. Only build deps can be used in build scripts
	pub build: HashSet<Dependency>,
}

/// # Single dependency
///
/// Struct for a singular dependency, containing all required info
pub struct Dependency {
	/// Dependency name
	pub name: String,
	/// Dependency kind. See [`DependencyKind`]
	pub kind: DependencyKind,
	/// Features for the dependency
	pub features: Vec<String>,
	/// Reasoning for the dependency (documentation)
	pub usage: Option<String>
}

impl Hash for Dependency {
	fn hash<H: Hasher>(&self, state: &mut H) {
		<String as Hash>::hash(&self.name, state)
	}
}

impl PartialEq for Dependency {
	fn eq(&self, other: &Self) -> bool {
		self.name == other.name
	}
}

impl Eq for Dependency {}

/// # Dependency kind
///
/// This specified what type of dependency is being specified
pub enum DependencyKind {
	/// Dependency with a [`SemVer`] version
	Versioned(SemVer),
	/// Dependency from a git repo
	Git {
		/// URL to the git repo
		git: String,
		/// Branch to use. Uses "main" if none is specified
		branch: Option<String>
	},
	/// Local dependency
	Path {
		/// Path to the dependency root with `.conic` manifest file in it
		path: PathBuf
	}
}
