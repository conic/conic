# Manifest manager

This module allows for parsing manifest files. That's about it. It uses [`serde`](https://crates.io/crates/serde) for deserialization.

For how fck manifest files are structured, see the [manifest file docs]() (unimplemented).

## Licenses

License IDs are from SPDX v3.21. These are stored in `src/licenses.rs` but can be updated using the `bump_license_list.sh` script using `jq`.
