//! Manifest file module

mod prelude;
mod semver;
mod serde;
mod licenses;
#[cfg(debug_assertions)]
mod display;
mod new;
mod checks;

pub use prelude::*;
