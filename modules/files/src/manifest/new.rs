use std::collections::HashMap;
use std::path::PathBuf;
use lang::prelude::LanguageRaw;
use crate::manifest::{Author, Manifest, Package, PackageType};
use crate::manifest::semver::SemVerValue;

pub struct NewOptions {
	kind: NewOptionKind,
	name: String,
	src: Option<PathBuf>,
	tests: Option<PathBuf>,
	benches: Option<PathBuf>,
}

pub enum NewOptionKind {
	Lib,
	App,
	LibApp
}

// impl Manifest {
// 	pub fn new(l: &LanguageRaw, cfg: ConfigFile, opts: NewOptions) -> Self {
// 		Self {
// 			package: Package {
// 				name: opts.name,
// 				src: opts.src.unwrap_or(PathBuf::from(&l.keywords.manifest_keys[2])),
// 				tests: opts.tests,
// 				benches: opts.benches,
// 				kind: match opts.kind {
// 					NewOptionKind::Lib => PackageType::Lib { path: PathBuf::from(l.keywords.manifest_keys[6]).with_extension("conic") },
// 					NewOptionKind::App => PackageType::App { path: PathBuf::from(l.keywords.manifest_keys[26]).with_extension("conic") },
// 					NewOptionKind::LibApp => PackageType::LibApp {
// 						lib_path: PathBuf::from(l.keywords.manifest_keys[6]).with_extension("conic"),
// 						app_path: PathBuf::from(l.keywords.manifest_keys[26]).with_extension("conic")
// 					NewOptionKind::Lib => PackageType::Lib { path: PathBuf::from(l.keywords.manifest_keys[6]).with_extension("fck") },
// 					NewOptionKind::App => PackageType::App { path: PathBuf::from(l.keywords.manifest_keys[26]).with_extension("fck") },
// 					NewOptionKind::LibApp => PackageType::LibApp {
// 						lib_path: PathBuf::from(l.keywords.manifest_keys[6]).with_extension("fck"),
// 						app_path: PathBuf::from(l.keywords.manifest_keys[26]).with_extension("fck")
// 					}
// 				},
// 				version: SemVerValue {
// 					version: (0, 1, 0),
// 					pre: None, meta: None
// 				},
// 				authors: HashMap::new(),
// 				license: None,
// 				description: None,
// 				readme: None,
// 				homepage: None,
// 				repo: None,
// 				features: Default::default(),
// 			},
// 			dependencies: Default::default(),
// 		}
// 	}
// }
//
// fn hashmap_union(primary: HashMap<String, Author>, secondary: HashMap<String, Author>) -> HashMap<String, Author> {
// 	todo!()
// }

impl Author {
	fn join(&mut self, other: Self) {
		macro_rules! replace_option {
		    ($($t:ident),*$(,)?) => {$(replace_option!(@inner, $t);)*};
		    (@inner, $t:ident$(,)?) => { if self.$t.is_none() { self.$t = other.$t } };
		}
		replace_option!(email, github, gitlab);
	}
}
