//! # Semantic Versioning
//!
//! This module contains all the structs to parse versions

/// # Semantic Versioning value or range
///
/// This is used for dependencies to specify the required version
#[derive(Debug)]
pub enum SemVer {
	/// Single (optionally) qualified version
	Single {
		/// Version qualifier
		qualifier: SemVerQualifier,
		/// Version value
		value: SemVerDepValue
	},
	/// Range of values
	Range(Vec<SemVerRangeValue>)
}

impl SemVer {
	/// Parse a semantic version from a string
	pub fn from_string(s: String) -> Option<Self> {
		if s.starts_with("^") {
			Some(Self::Single {
				qualifier: SemVerQualifier::Above,
				value: SemVerDepValue::parse(&s[1..])?
			})
		} else if s.starts_with("~") {
			Some(Self::Single {
				qualifier: SemVerQualifier::Minimum,
				value: SemVerDepValue::parse(&s[1..])?
			})
		} else if s.ends_with("*") {
			Some(Self::Single {
				qualifier: SemVerQualifier::Wild,
				value: SemVerDepValue::parse(&s[..s.len() - 1])?
			})
		} else if s.contains("=") || s.contains("<") || s.contains(">") {
			let mut range = Vec::new();
			for i in s.split(",").map(|t| t.trim()) {
				if i.starts_with("<=") {
					range.push(SemVerRangeValue {
						range: SemVerRangeQualifier::LTE,
						value: SemVerDepValue::parse(&i[2..])?
					})
				} else if i.starts_with(">=") {
					range.push(SemVerRangeValue {
						range: SemVerRangeQualifier::GTE,
						value: SemVerDepValue::parse(&i[2..])?
					})
				} else if i.starts_with("<") {
					range.push(SemVerRangeValue {
						range: SemVerRangeQualifier::LT,
						value: SemVerDepValue::parse(&i[1..])?
					})
				} else if i.starts_with(">") {
					range.push(SemVerRangeValue {
						range: SemVerRangeQualifier::GT,
						value: SemVerDepValue::parse(&i[1..])?
					})
				} else if i.starts_with("=") {
					range.push(SemVerRangeValue {
						range: SemVerRangeQualifier::Eq,
						value: SemVerDepValue::parse(&i[1..])?
					})
				} else {
					range.push(SemVerRangeValue {
						range: SemVerRangeQualifier::None,
						value: SemVerDepValue::parse(i)?
					})
				}
			}
			Some(Self::Range(range))
		} else {
			Some(Self::Single {
				qualifier: SemVerQualifier::None,
				value: SemVerDepValue::parse(&*s)?
			})
		}
	}
}

/// # SemVer value
///
/// Contains a semantic versioning valid version number
#[derive(Debug)]
pub struct SemVerValue {
	/// Major, minor, and patch version
	pub(crate) version: (u8, u8, u8),
	/// Pre-release value
	pub(crate) pre: Option<String>,
	/// Metadata value
	pub(crate) meta: Option<String>
}

impl Default for SemVerValue {
	fn default() -> Self {
		Self {
			version: (u8::MAX, u8::MAX, u8::MAX),
			pre: None,
			meta: None
		}
	}
}

impl SemVerValue {
	pub fn from_string(s: String) -> Option<Self> {
		let mut chars = s.chars();
		let mut major = String::from(chars.next()?);
		while let Some(c) = chars.next() {
			if c == '.' { break }
			major.push(c)
		}
		let major = u8::from_str_radix(&*major, 10).ok()?;
		let mut minor = String::from(chars.next()?);
		while let Some(c) = chars.next() {
			if c == '.' { break }
			minor.push(c)
		}
		let minor = u8::from_str_radix(&*minor, 10).ok()?;
		let mut patch = String::from(chars.next()?);
		let mut has_pre = false;
		let mut has_meta = false;
		while let Some(c) = chars.next() {
			if c == '-' { has_pre = true; break }
			if c == '+' { has_meta = true; break }
			patch.push(c)
		}
		let patch = u8::from_str_radix(&*patch, 10).ok()?;
		let pre;
		let meta;
		if has_pre {
			let mut pre_val = String::from(chars.next()?);
			while let Some(c) = chars.next() {
				if c == '+' { has_meta = true; break }
				pre_val.push(c)
			}
			pre = Some(pre_val)
			
		} else { pre = None }
		if has_meta {
			let meta_val = String::from_iter(chars);
			if meta_val.is_empty() { return None }
			meta = Some(meta_val)
		} else { meta = None }
		Some(Self {
			version: (major, minor, patch),
			pre, meta
		})
	}
}

/// # SemVer qualifier
///
/// This specifies how to match a SemVer value
#[derive(Debug)]
pub enum SemVerQualifier {
	/// Match the version exactly
	None,
	/// Match any version above the value (`^`)
	Above,
	/// Minimum specified value (`~`):
	///
	/// | Version  | Range equivalent  |
	/// |----------|-------------------|
	/// | `~1.2.3` | `>=1.2.3, <1.3.0` |
	/// | `~1.2`   | `>=1.2.0, <1.3.0` |
	/// | `~1`     | `>=1.0.0, <2.0.0` |
	Minimum,
	/// Match. Indicates the last version number was a wildcard (`*`)
	Wild
}

/// # SemVer value
///
/// Contains a semantic versioning valid version number for dependencies
#[derive(Debug)]
pub enum SemVerDepValue {
	/// Only a major version specified
	Major(u8),
	/// Major and minor versions specified
	Minor(u8, u8),
	/// Major, minor, and patch versions specified with optional pre-release and metadata values
	Additional {
		/// Major, minor, and patch versions
		version: (u8, u8, u8),
		/// Pre-release value
		pre: Option<String>,
		/// Metadata value
		meta: Option<String>
	}
}

impl SemVerDepValue {
	fn parse(s: &str) -> Option<Self> {
		let mut chars = s.chars().peekable();
		let mut major = String::from(chars.next()?);
		while let Some(c) = chars.next() {
			if c == '.' { break }
			major.push(c)
		}
		let major = u8::from_str_radix(&*major, 10).ok()?;
		if chars.peek().is_none() {
			return Some(Self::Major(major))
		}
		let mut minor = String::from(chars.next()?);
		while let Some(c) = chars.next() {
			if c == '.' { break }
			minor.push(c)
		}
		let minor = u8::from_str_radix(&*minor, 10).ok()?;
		if chars.peek().is_none() {
			return Some(Self::Minor(major, minor))
		}
		let mut patch = String::from(chars.next()?);
		let mut has_pre = false;
		let mut has_meta = false;
		while let Some(c) = chars.next() {
			if c == '-' { has_pre = true; break }
			if c == '+' { has_meta = true; break }
			patch.push(c)
		}
		let patch = u8::from_str_radix(&*patch, 10).ok()?;
		if chars.peek().is_none() && !has_meta && !has_pre {
			return Some(Self::Additional { version: (major, minor, patch), pre: None, meta: None })
		}
		let pre;
		let meta;
		if has_pre {
			let mut pre_val = String::from(chars.next()?);
			while let Some(c) = chars.next() {
				if c == '+' { has_meta = true; break }
				pre_val.push(c)
			}
			pre = Some(pre_val)
			
		} else { pre = None }
		if has_meta {
			let meta_val = String::from_iter(chars);
			if meta_val.is_empty() { return None }
			meta = Some(meta_val)
		} else { meta = None }
		Some(Self::Additional {
			version: (major, minor, patch),
			pre, meta
		})
	}
}

/// # Range of SemVer version with range qualifier
///
/// Contains a singe range qualified SemVer
#[derive(Debug)]
pub struct SemVerRangeValue {
	pub range: SemVerRangeQualifier,
	pub value: SemVerDepValue
}

#[derive(Debug)]
pub enum SemVerRangeQualifier {
	#[doc("Less than (`<`)")]LT, #[doc("Greater than (`>`)")]GT,
	#[doc("Less than or equal (`<=`)")]LTE, #[doc("Greater than or equal (`>=`)")]GTE,
	#[doc("Equal (`=`)")]Eq, #[doc("No qualifier")]None
}
