use crate::manifest::{Dependencies, DependenciesInner, DependencyKind, Manifest, Package};
use crate::manifest::serde::ManifestParseError;

impl Manifest {
	/// Check a manifest package for correctness
	pub fn check(&self) -> Result<(), ManifestParseError> {
		let kwds = lang::get_no_buffer(&*self.language).unwrap().keywords.manifest_keys.map(|t| t.to_string());
		self.package.check(&kwds)?;
		self.dependencies.check(&kwds)
	}
}

impl Package {
	/// Check the following:
	/// - `src` exists, is relative, and is a directory
	/// - `test` exists, is relative, and is a directory
	/// - `bench` exists, is relative, and is a directory
	fn check(&self, kwds: &[String; 27]) -> Result<(), ManifestParseError> {
		if !self.src.is_dir() { return Err(ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[0], kwds[2]))) }
		if !self.src.is_relative() { return Err(ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[0], kwds[2]))) }
		if !self.src.exists() { return Err(ManifestParseError::MissingFile(format!("{}.{}", kwds[0], kwds[4]), self.src.clone())) }
		if let Some(tests) = &self.tests {
			if !tests.is_dir() { return Err(ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[0], kwds[3]))) }
			if !tests.is_relative() { return Err(ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[0], kwds[3]))) }
			if !tests.exists() { return Err(ManifestParseError::MissingFile(format!("{}.{}", kwds[0], kwds[4]), tests.clone())) }
		}
		if let Some(tests) = &self.benches {
			if !tests.is_dir() { return Err(ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[0], kwds[4]))) }
			if !tests.is_relative() { return Err(ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[0], kwds[4]))) }
			if !tests.exists() { return Err(ManifestParseError::MissingFile(format!("{}.{}", kwds[0], kwds[4]), tests.clone())) }
		}
		Ok(())
	}
}

impl Dependencies {
	/// Check all dependencies are valid
	fn check(&self, kwds: &[&String; 27]) -> Result<(), ManifestParseError> {
		self.dependencies.check(kwds)?;
		for (_, target_deps) in self.targets.iter() {
			target_deps.check(kwds)?
		}
		Ok(())
	}
}

impl DependenciesInner {
	/// Check for each dependency:
	/// - if it's a path dependency, check the path exists and is a directory
	fn check(&self, kwds: &[&String; 27]) -> Result<(), ManifestParseError> {
		for dep in self.base.iter() {
			if let DependencyKind::Path { path } = &dep.kind {
				if !path.is_dir() { return Err(ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[0], kwds[4]))) }
				if !path.exists() { return Err(ManifestParseError::MissingFile(format!("{}.{}", kwds[0], kwds[4]), path.clone())) }
			}
		}
		Ok(())
	}
}
