use std::cmp::Ordering;
use std::collections::{HashMap, HashSet};
use std::fmt::Display;
use std::iter::Peekable;
use std::path::PathBuf;
use super::{
	Author, Dependencies, DependenciesInner, Dependency, DependencyKind, License, Manifest, Package, PackageType,
	licenses::LICENSES,
	semver::{SemVer, SemVerValue}
};

#[derive(Debug, Clone)]
struct Line {
	indent: u8,
	contents: LineContents
}

#[derive(PartialEq, Debug, Clone)]
enum LineContents {
	/// Single value with *no* ':' at the end
	Singular(String),
	/// Single value with ':' at the end
	SingularParent(String),
	/// Single value with ': |' at the end
	SingularParentLongString(String),
	/// Pair of values with a ':' separating them
	Pair(String, String)
}

#[derive(Debug)]
pub enum ManifestParseError {
	UnknownKey(String),
	MissingValue(String),
	MissingFile(String, PathBuf),
	IncorrectFormat(String),
	UnexpectedValue(String, String),
	NoData,
	MalformedFile,
	MissingLang,
	UnknownLang
}

impl Manifest {
	pub fn from_string(s: String) -> Result<Self, ManifestParseError> {
		let lines = s.lines()
			.filter(|t| {
				let trimmed = t.trim();
				!(trimmed.starts_with("//") || trimmed.is_empty())
			})
			.collect::<Vec<_>>();
		if lines.is_empty() { return Err(ManifestParseError::NoData) }

		let (lang, raw_lines) = lines.split_first().unwrap();
		if !lang.starts_with("!!") { return Err(ManifestParseError::MissingLang) }
		// There is probably a much higher lower bound, but 5 works fine for what I want it to do
		if raw_lines.len() < 5 { return Err(ManifestParseError::NoData) }

		let kwds: [String; 27] = match lang::get_no_buffer(&lang[2..]) {
			Some((l, _)) => l.keywords.manifest_keys.map(|t| t.to_string()),
			_ => return Err(ManifestParseError::UnknownLang)
		};

		let mut lines = Vec::new();
		let mut indent_count = 0;

		for line in raw_lines {
			let mut line_indent = 0;
			let mut char_iter = line.chars().peekable();
			while let Some(c) = char_iter.peek() {
				match *c {
					' ' => line_indent += 1,
					'\t' => line_indent += 2,
					_ => break
				}
				char_iter.next();
			}
			let indent = line_indent / 2;
			indent_count = line_indent;
			let mut name = String::new();
			while let Some(c) = char_iter.peek() {
				if *c == ':' { break }
				name.push(*c);
				char_iter.next();
			}
			if char_iter.peek() == None {
				lines.push(Line {
					indent,
					contents: LineContents::Singular(name.trim().to_string())
				});
				continue
			}
			char_iter.next();
			while let Some(c) = char_iter.peek() {
				if *c != ' ' { break }
				char_iter.next();
			}
			match char_iter.peek() {
				None => {
					lines.push(Line {
						indent,
						contents: LineContents::SingularParent(name.trim().to_string())
					});
				}
				Some(&'|') => {
					lines.push(Line {
						indent,
						contents: LineContents::SingularParentLongString(name.trim().to_string())
					});
				}
				_ => {
					let val = String::from_iter(char_iter);
					lines.push(Line {
						indent,
						contents: LineContents::Pair(name.trim().to_string(), val.trim().to_string())
					});
				}
			}
		}

		let mut filtered_lines = Vec::new();
		let mut lines = lines.iter().peekable();

		while let Some(line) = lines.next() {
			if let LineContents::SingularParentLongString(key) = &line.contents {
				let mut value = Vec::new();
				while let Some(sub_line) = lines.peek() {
					if sub_line.indent <= line.indent { break }
					let sub_line = lines.next().unwrap();
					if let LineContents::Singular(s_val) = &sub_line.contents { value.push(s_val.clone()) } else {
						return Err(ManifestParseError::MalformedFile)
					}
				}
				filtered_lines.push(Line {
					indent: line.indent,
					contents: LineContents::Pair(key.clone(), value.join("\n"))
				})
			} else {
				filtered_lines.push(line.clone())
			}
		}

		// check for non-repeated values
		let mut key_stack = vec![HashSet::new()];
		for line in filtered_lines.iter() {
			match (key_stack.len() - 1).cmp(&(line.indent as usize)) {
				Ordering::Less => for _ in (key_stack.len() - 1)..line.indent as usize { key_stack.push(HashSet::new()); }
				Ordering::Equal => {}
				Ordering::Greater => for _ in line.indent as usize..(key_stack.len() - 1) { key_stack.pop(); }
			}
			let key = match &line.contents {
				LineContents::Singular(k) => k.clone(),
				LineContents::SingularParent(k) => k.clone(),
				LineContents::SingularParentLongString(k) => k.clone(),
				LineContents::Pair(k, _) => k.clone()
			};
			if !key_stack.last_mut().unwrap().insert(key) {
				return Err(ManifestParseError::MalformedFile)
			}
		}

		let mut lines = filtered_lines.iter().peekable();
		let mut out: Self = lang[2..].to_string().into();

		while let Some(line) = lines.next() {
			if line.contents == LineContents::SingularParent(kwds[0].clone()) {
				Package::parse(&mut out.package, &mut lines, &kwds)?
			} else if line.contents == LineContents::SingularParent(kwds[19].clone()) {
				Dependencies::parse(&mut out.dependencies, &mut lines, &kwds)?
			} else { return Err(ManifestParseError::UnknownKey("Root".to_string())) }
		}

		Ok(out)
	}
}

impl Package {
	fn parse<'a, T: Iterator<Item=&'a Line>>(&mut self, lines: &mut Peekable<T>, kwds: &[String; 27]) -> Result<(), ManifestParseError> {
		while let Some(line) = lines.peek() {
			if line.indent == 0 { break }
			let line = lines.next().unwrap();
			match &line.contents {
				LineContents::Singular(s) => { return Err(ManifestParseError::UnexpectedValue(kwds[0].clone(), s.clone())) }
				LineContents::SingularParent(key) => {
					if *key == kwds[5] { // type
						let inner = match lines.next() {
							None => { return Err(ManifestParseError::MissingValue(format!("{}.{}.{{{} | {}}}", kwds[0], kwds[5], kwds[6], kwds[7]))) }
							Some(l) => l
						};
						if inner.indent != 2 { return Err(ManifestParseError::MissingValue(format!("{}.{}.{{{} | {}}}", kwds[0], kwds[5], kwds[6], kwds[7]))) }
						if let LineContents::Pair(key, val) = &inner.contents {
							if *key == kwds[6] {
								self.kind = PackageType::Lib { path: PathBuf::from(val) }
							} else if *key == kwds[7] {
								self.kind = PackageType::App { path: PathBuf::from(val) }
							} else {
								return Err(ManifestParseError::UnknownKey(format!("{}.{}.{}", kwds[0], kwds[5], key)))
							}
						}
						let inner = match lines.peek() {
							None => { continue }
							Some(l) => l
						};
						if inner.indent != 2 { continue }
						let inner = lines.next().unwrap();
						if let LineContents::Pair(key, val) = &inner.contents {
							if *key != kwds[6] && *key != kwds[7] { return Err(ManifestParseError::UnknownKey(format!("{}.{}.{}", kwds[0], kwds[5], key))) }
							self.kind.extend(PathBuf::from(val));
						}
					} else if *key == kwds[18] {
						while let Some(feature_line) = lines.peek() {
							if feature_line.indent <= line.indent { break }
							let feature_line = lines.next().unwrap();
							match &feature_line.contents {
								LineContents::Singular(k) => {
									if k.contains(" ") { return Err(ManifestParseError::IncorrectFormat(format!("{}.{}.{}", kwds[0], kwds[18], k))) }
									self.features.insert(k.clone(), None);
								}
								LineContents::Pair(k, v) => {
									if k.contains(" ") { return Err(ManifestParseError::IncorrectFormat(format!("{}.{}.{}", kwds[0], kwds[18], k))) }
									self.features.insert(k.clone(), Some(v.clone()));
								}
								_ => return Err(ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[0], kwds[18])))
							}
						}
					} else if *key == kwds[9] { // authors
						while let Some(author_line) = lines.peek() {
							if author_line.indent < 2 { break }
							let author_line = lines.next().unwrap();
							match &author_line.contents {
								LineContents::Singular(name) => {
									self.authors.insert(name.clone(), Author::default());
									continue
								}
								LineContents::SingularParent(name) => {
									self.authors.insert(name.clone(), Author::parse(lines, kwds)?);
								}
								LineContents::SingularParentLongString(_) => unreachable!(),
								LineContents::Pair(name, _) => { return Err(ManifestParseError::IncorrectFormat(format!("{}.{}.{}", kwds[0], kwds[9], name))) }
							}
						}
					} else {
						return Err(ManifestParseError::UnknownKey(format!("{}.{}", kwds[0], key)))
					}
				}
				LineContents::SingularParentLongString(_) => unreachable!(),
				LineContents::Pair(key, value) => {
					if *key == kwds[1] { // name
						if value.contains(" ") { return Err(ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[0], kwds[1]))) }
						self.name = value.clone();
					} else if *key == kwds[8] { // version
						self.version = SemVerValue::from_string(value.clone()).ok_or(ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[0], kwds[8])))?
					} else if *key == kwds[13] { // license
						if let Some((_, name, _)) = LICENSES.iter().find(|(n, _, _)| &*value == *n) {
							self.license = Some(License::Named { id: value.clone(), name: name.to_string() })
						} else {
							self.license = Some(License::File(PathBuf::from(value)))
						}
					} else if *key == kwds[14] { // description
						self.description = Some(value.clone())
					} else if *key == kwds[18] { // features
						self.features.insert(value.clone(), None);
					} else if *key == kwds[2] { // src
						self.src = PathBuf::from(value)
					} else if *key == kwds[3] { // tests
						self.tests = Some(PathBuf::from(value))
					} else if *key == kwds[4] { // benches
						self.benches = Some(PathBuf::from(value))
					} else {
						return Err(ManifestParseError::UnknownKey(format!("{}.{}", kwds[0], key)))
					}
				}
			}
		}

		if self.name.is_empty() { return Err(ManifestParseError::MissingValue(format!("{}.{}", kwds[0], kwds[1]))) }
		if self.src == PathBuf::new() { return Err(ManifestParseError::MissingValue(format!("{}.{}", kwds[0], kwds[2]))) }
		if !self.src.is_dir() { return Err(ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[0], kwds[2]))) }
		if !self.src.is_relative() { return Err(ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[0], kwds[2]))) }
		if !self.src.exists() { return Err(ManifestParseError::MissingFile(format!("{}.{}", kwds[0], kwds[4]), self.src.clone())) }
		if let Some(tests) = &self.tests {
			if *tests == PathBuf::new() { return Err(ManifestParseError::MissingValue(format!("{}.{}", kwds[0], kwds[3]))) }
			if !tests.is_dir() { return Err(ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[0], kwds[3]))) }
			if !tests.is_relative() { return Err(ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[0], kwds[3]))) }
			if !tests.exists() { return Err(ManifestParseError::MissingFile(format!("{}.{}", kwds[0], kwds[4]), tests.clone())) }
		}
		if let Some(tests) = &self.benches {
			if *tests == PathBuf::new() { return Err(ManifestParseError::MissingValue(format!("{}.{}", kwds[0], kwds[4]))) }
			if !tests.is_dir() { return Err(ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[0], kwds[4]))) }
			if !tests.is_relative() { return Err(ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[0], kwds[4]))) }
			if !tests.exists() { return Err(ManifestParseError::MissingFile(format!("{}.{}", kwds[0], kwds[4]), tests.clone())) }
		}
		if self.version.version == (u8::MAX, u8::MAX, u8::MAX) { return Err(ManifestParseError::MissingValue(format!("{}.{}", kwds[0], kwds[8]))) }
		macro_rules! check_is_conic_file {
		    ($f:ident, $i:literal) => {
				if !self.src.join($f).exists() { return Err(ManifestParseError::MissingFile(format!("{}.{}.{}", kwds[0], kwds[5], kwds[$i]), $f.clone())) }
				if !self.src.join($f).is_file() { return Err(ManifestParseError::IncorrectFormat(format!("{}.{}.{}", kwds[0], kwds[5], kwds[$i]))) }
				if let Some(ext) = $f.extension() {
					if ext != "cn" { return Err(ManifestParseError::IncorrectFormat(format!("{}.{}.{}", kwds[0], kwds[5], kwds[$i]))) }
				} else {
					return Err(ManifestParseError::IncorrectFormat(format!("{}.{}.{}", kwds[0], kwds[5], kwds[$i])))
				}
			};
		}

		Ok(())
	}
}

impl Author {
	fn parse<'a, T: Iterator<Item=&'a Line>>(lines: &mut Peekable<T>, kwds: &[String; 27]) -> Result<Self, ManifestParseError> {
		let mut out = Self::default();
		while let Some(info_line) = lines.peek() {
			if info_line.indent < 3 { break }
			let info_line = lines.next().unwrap();
			match &info_line.contents {
				LineContents::Singular(k) => return Err(ManifestParseError::UnknownKey(format!("{}.{}.*.{}", kwds[0], kwds[9], k))),
				LineContents::SingularParent(k) => return Err(ManifestParseError::UnknownKey(format!("{}.{}.*.{}", kwds[0], kwds[9], k))),
				LineContents::SingularParentLongString(_) => unreachable!(),
				LineContents::Pair(k, v) => {
					if *k == kwds[10] {
						out.github = Some(v.clone())
					} else if *k == kwds[11] {
						out.gitlab = Some(v.clone())
					} else if *k == kwds[12] {
						out.email = Some(v.clone())
					} else {
						return Err(ManifestParseError::UnknownKey(format!("{}.{}.*.{}", kwds[0], kwds[9], k)))
					}
				}
			}
		}
		Ok(out)
	}
}

impl Dependencies {
	fn parse<'a, T: Iterator<Item=&'a Line>>(&mut self, lines: &mut Peekable<T>, kwds: &[String; 27]) -> Result<(), ManifestParseError> {
		let mut depth = 2;
		let mut dependencies_ref_set = &mut self.dependencies.base;
		let mut dependency_target = String::new();
		while let Some(line) = lines.peek() {
			if line.indent == 0 { break }
			if line.indent == 1 && depth >= 3 {
				dependencies_ref_set = &mut self.dependencies.base;
				depth = 2;
			}
			let line = lines.next().unwrap();
			match &line.contents {
				LineContents::Singular(key) => {
					if key.contains("@") {
						let split: [&str; 2] = key.split("@").collect::<Vec<_>>().try_into().map_err(|_| ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[19], key)))?;
						if split[0].contains(" ") { return Err(ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[19], key))) }
						let ver = SemVer::from_string(split[1].to_string()).ok_or(ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[19], key)))?;
						dependencies_ref_set.insert(Dependency {
							name: split[0].to_string(),
							kind: DependencyKind::Versioned(ver),
							features: Vec::new(),
							usage: None
						});
					} else {
						return Err(ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[19], key)))
					}
				}
				LineContents::SingularParent(key) => {
					if key.contains("@") {
						let split: [&str; 2] = key.split("@").collect::<Vec<_>>().try_into().map_err(|_| ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[19], key)))?;
						if split[0].contains(" ") { return Err(ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[19], key))) }

						// get all data under dependency
						let mut data = HashMap::new();
						while let Some(data_line) = lines.peek() {
							if data_line.indent < depth { break }
							let data_line = lines.next().unwrap();
							match &data_line.contents {
								LineContents::Singular(k) => return Err(ManifestParseError::UnknownKey(format!("{}.{}.{}", kwds[19], split[0], k))),
								LineContents::SingularParent(k) => return Err(ManifestParseError::UnknownKey(format!("{}.{}.{}", kwds[19], split[0], k))),
								LineContents::SingularParentLongString(_) => unreachable!(),
								LineContents::Pair(k, v) => { data.insert(k.clone(), v.clone()); }
							}
						}

						let mut dep = if split[1] == &*kwds[21] { // git
							Dependency {
								name: split[0].to_string(),
								kind: DependencyKind::Git {
									git: data.remove(&kwds[21]).ok_or(ManifestParseError::MissingValue(format!("{}.{}.{}", kwds[19], split[0], kwds[21])))?,
									branch: data.remove(&kwds[22])
								},
								features: Vec::new(),
								usage: None
							}
						} else if split[1] == &*kwds[23] { // path
							Dependency {
								name: split[0].to_string(),
								kind: DependencyKind::Path {
									path: data.remove(&kwds[23]).map(PathBuf::from).ok_or(ManifestParseError::MissingValue(format!("{}.{}.{}", kwds[19], split[0], kwds[23])))?
								},
								features: Vec::new(),
								usage: None
							}
						} else {
							let ver = SemVer::from_string(split[1].to_string()).ok_or(ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[19], key)))?;
							Dependency {
								name: split[0].to_string(),
								kind: DependencyKind::Versioned(ver),
								features: Vec::new(),
								usage: None
							}
						};

						for (k, v) in data {
							if k == kwds[18] { // features
								dep.features = v.split(",").map(|t| t.trim().to_string()).collect()
							} else if k == kwds[20] { // usage
								dep.usage = Some(v)
							} else {
								return Err(ManifestParseError::UnknownKey(format!("{}.{}.{}", kwds[19], split[0], k)))
							}
						}

						dependencies_ref_set.insert(dep);
					} else if *key == kwds[24] && depth == 2 {
						dependencies_ref_set = &mut self.dependencies.dev;
						depth = 3;
					} else if *key == kwds[24] && depth == 3 {
						dependencies_ref_set = &mut self.targets.get_mut(&dependency_target).unwrap().dev;
						depth = 4;
					} else if *key == kwds[25] && depth == 2 {
						dependencies_ref_set = &mut self.dependencies.build;
						depth = 3;
					} else if *key == kwds[25] && depth == 3 {
						dependencies_ref_set = &mut self.targets.get_mut(&dependency_target).unwrap().build;
						depth = 4;
					} else if depth == 2 {
						self.targets.insert(key.clone(), Default::default());
						dependencies_ref_set = &mut self.targets.get_mut(key).unwrap().base;
						dependency_target = key.clone();
						depth = 3;
					} else {
						return Err(ManifestParseError::IncorrectFormat(format!("{}.{}", kwds[19], key)))
					}
				}
				LineContents::SingularParentLongString(_) => unreachable!(),
				LineContents::Pair(k, _) => return Err(ManifestParseError::UnknownKey(format!("{}.{}", &kwds[19], k)))
			}
		}
		Ok(())
	}
}

impl From<String> for Manifest {
	fn from(value: String) -> Self {
		Self {
			language: value,
			package: Package::default(),
			dependencies: Dependencies::default()
		}
	}
}

impl Default for Package {
	fn default() -> Self {
		Self {
			name: String::new(),
			src: PathBuf::new(),
			tests: None,
			benches: None,
			kind: PackageType::Lib { path: PathBuf::new() },
			version: Default::default(),
			authors: Default::default(),
			license: None,
			description: None,
			readme: None,
			homepage: None,
			repo: None,
			features: Default::default(),
		}
	}
}

impl Default for Author {
	fn default() -> Self {
		Self {
			email: None,
			github: None,
			gitlab: None
		}
	}
}

impl Default for Dependencies {
	fn default() -> Self {
		Self {
			dependencies: Default::default(),
			targets: Default::default(),
		}
	}
}

impl Default for DependenciesInner {
	fn default() -> Self {
		Self {
			base: Default::default(),
			dev: Default::default(),
			build: Default::default(),
		}
	}
}
