use std::fmt::{Display, Formatter};
use crate::manifest::{Author, Dependencies, DependenciesInner, Dependency, DependencyKind, License, Manifest, Package, PackageType};
use crate::manifest::semver::{SemVer, SemVerDepValue, SemVerQualifier, SemVerRangeQualifier, SemVerRangeValue, SemVerValue};

impl Display for Manifest {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		write!(f, "{}", self.package)?;
		write!(f, "{:1}", self.dependencies)
	}
}

impl Display for Package {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		writeln!(f, "package:")?;
		writeln!(f, "\tname: {}", self.name)?;
		writeln!(f, "\tsrc: {}", self.src.display())?;
		if let Some(tests) = &self.tests {
			writeln!(f, "\ttests: {}", tests.display())?;
		}
		if let Some(tests) = &self.benches {
			writeln!(f, "\ttests: {}", tests.display())?;
		}
		write!(f, "\ttype:\n{}", self.kind)?;
		writeln!(f, "\tversion: {}", self.version)?;
		if !self.authors.is_empty() {
			writeln!(f, "\tauthors:")?;
			for (name, data) in self.authors.iter() {
				if data.email == None && data.github == None && data.gitlab == None {
					writeln!(f, "\t\t{}", name)?;
				} else {
					write!(f, "\t\t{}:\n{}", name, data)?;
				}
			}
		}
		if let Some(license) = &self.license {
			writeln!(f, "\tlicense: {}", license)?;
		}
		if let Some(desc) = &self.description {
			writeln!(f, "\tdescription: {:?}", desc)?;
		}
		if let Some(readme) = &self.readme {
			writeln!(f, "\treadme: {:?}", readme)?;
		}
		if let Some(homepage) = &self.homepage {
			writeln!(f, "\thomepage: {:?}", homepage)?;
		}
		if let Some(repo) = &self.repo {
			writeln!(f, "\trepo: {:?}", repo)?;
		}
		if !self.features.is_empty() {
			writeln!(f, "\tfeatures:")?;
			for (name, data) in self.features.iter() {
				if let Some(data) = data {
					writeln!(f, "\t\t{}: {}", name, data)?;
				} else {
					writeln!(f, "\t\t{}", name)?;
 				}
			}
		}
		Ok(())
	}
}

impl Display for Author {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		if let Some(email) = &self.email {
			writeln!(f, "\t\t\temail: {}", email)?;
		}
		if let Some(github) = &self.github {
			writeln!(f, "\t\t\tgithub: {}", github)?;
		}
		if let Some(gitlab) = &self.gitlab {
			writeln!(f, "\t\t\tgitlab: {}", gitlab)?;
		}
		Ok(())
	}
}

impl Display for PackageType {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		match self {
			PackageType::Lib { path } => writeln!(f, "\t\tlib: {}", path.display()),
			PackageType::App { path } => writeln!(f, "\t\tapp: {}", path.display()),
			PackageType::LibApp { lib_path, app_path } => writeln!(f, "\t\tlib: {}\n\t\tapp: {}", lib_path.display(), app_path.display()),
		}
	}
}

impl Display for SemVerValue {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		write!(f, "{}.{}.{}", self.version.0, self.version.1, self.version.2)?;
		if let Some(pre) = &self.pre {
			write!(f, "-{}", pre)?;
		}
		if let Some(meta) = &self.meta {
			write!(f, "+{}", meta)?;
		}
		Ok(())
	}
}

impl Display for License {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		match self {
			License::Named { name, id } => write!(f, "{} ({})", id, name),
			License::File(path) => write!(f, "File: {}", path.display())
		}
	}
}

impl Display for Dependencies {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		let mut w = f.width().unwrap_or(0);
		writeln!(f, "dependencies:")?;
		write!(f, "{:w$}", self.dependencies)?;
		w += 1;
		for (k, v) in self.targets.iter() {
			writeln!(f, "\t{}:", k)?;
			write!(f, "{:w$}", v)?;
		}
		Ok(())
	}
}

impl Display for DependenciesInner {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		let mut w = f.width().unwrap_or(0);
		for i in self.base.iter() {
			write!(f, "{:w$}", i)?;
		}
		w += 1;
		if !self.dev.is_empty() {
			writeln!(f, "{}dev:", "\t".repeat(w - 1))?;
			for i in self.dev.iter() {
				write!(f, "{:w$}", i)?;
			}
		}
		if !self.build.is_empty() {
			writeln!(f, "{}build:", "\t".repeat(w - 1))?;
			for i in self.build.iter() {
				write!(f, "{:w$}", i)?;
			}
		}
		Ok(())
	}
}

impl Display for Dependency {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		let w = f.width().unwrap_or(0);
		match &self.kind {
			DependencyKind::Versioned(ver) => {
				writeln!(f, "{}{}@{}{}", "\t".repeat(w), self.name, ver, if self.features.is_empty() && self.usage.is_none() { "" } else { ":" })?;
				if !self.features.is_empty() {
					writeln!(f, "{}features: {}", "\t".repeat(w + 1), self.features.join(", "))?;
				}
				if let Some(usage) = &self.usage {
					writeln!(f, "{}usage: {}", "\t".repeat(w + 1), usage)?;
				}
			},
			DependencyKind::Git { git, branch } => {
				writeln!(f, "{}{}@git:", "\t".repeat(w), self.name)?;
				writeln!(f, "{}git: {}", "\t".repeat(w + 1), git)?;
				if let Some(usage) = branch {
					writeln!(f, "{}branch: {}", "\t".repeat(w + 1), usage)?;
				}
				if !self.features.is_empty() {
					writeln!(f, "{}features: {}", "\t".repeat(w + 1), self.features.join(", "))?;
				}
				if let Some(usage) = &self.usage {
					writeln!(f, "{}usage: {}", "\t".repeat(w + 1), usage)?;
				}
			}
			DependencyKind::Path { path } => {
				writeln!(f, "{}{}@path:", "\t".repeat(w), self.name)?;
				writeln!(f, "{}path: {}", "\t".repeat(w + 1), path.display())?;
				if !self.features.is_empty() {
					writeln!(f, "{}features: {}", "\t".repeat(w + 1), self.features.join(", "))?;
				}
				if let Some(usage) = &self.usage {
					writeln!(f, "{}usage: {}", "\t".repeat(w + 1), usage)?;
				}
			}
		}
		Ok(())
	}
}

impl Display for SemVer {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		match self {
			SemVer::Single { qualifier, value } => {
				match qualifier {
					SemVerQualifier::None => write!(f, "{}", value),
					SemVerQualifier::Above => write!(f, "^{}", value),
					SemVerQualifier::Minimum => write!(f, "~{}", value),
					SemVerQualifier::Wild => write!(f, "{}.*", value)
				}
			}
			SemVer::Range(r) => write!(f, "{}", r.iter().map(|t| format!("{}", t)).collect::<Vec<_>>().join(", "))
		}
	}
}

impl Display for SemVerRangeValue {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		let pre = match self.range {
			SemVerRangeQualifier::LT => "<",
			SemVerRangeQualifier::GT => ">",
			SemVerRangeQualifier::LTE => "<=",
			SemVerRangeQualifier::GTE => ">=",
			SemVerRangeQualifier::Eq => "=",
			SemVerRangeQualifier::None => ""
		};
		write!(f, "{}{}", pre, self.value)
	}
}

impl Display for SemVerDepValue {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		match self {
			SemVerDepValue::Major(m) => write!(f, "{}", m),
			SemVerDepValue::Minor(m1, m2) => write!(f, "{}.{}", m1, m2),
			SemVerDepValue::Additional { version, pre, meta } => {
				write!(f, "{}.{}.{}", version.0, version.1, version.2)?;
				if let Some(pre) = pre {
					write!(f, "-{}", pre)?;
				}
				if let Some(meta) = meta {
					write!(f, "+{}", meta)?;
				}
				Ok(())
			}
		}
	}
}
