use crate::manifest::Author as AuthorMeta;

#[cfg_attr(debug_assertions, derive(Debug))]
pub struct ConfigFile {
	default_lang: Option<String>,
	author: Option<Author>
}

#[cfg_attr(debug_assertions, derive(Debug))]
pub struct Author {
	name: String,
	meta: Option<AuthorMeta>
}

impl From<String> for ConfigFile {
	fn from(value: String) -> Self {
		todo!()
	}
}
