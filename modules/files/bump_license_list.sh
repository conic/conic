#!/bin/bash
# parse version from first arg or use default
default="3.21"
VER=${1:-$default}

# print help info
if [ "$VER" == "-h" ]; then
  echo -e "Download the licenses list and update src/licenses.rs\nDefaults to version $default unless specified";
  exit 0
fi

# Check for required commands
for c in jq curl printf ; do
  if ! command -v $c > /dev/null; then echo "$c not installed. Please install $c and re-run this script"; exit 1; fi
done

echo -n "Downloading licenses v"$VER"..."
CURL=$(curl "https://raw.githubusercontent.com/spdx/license-list-data/v"$VER"/json/licenses.json" --silent)
if [ $? -ne 0 ]; then
  echo " Failed"
  echo " curl returned 404 for version $VER and url https://raw.githubusercontent.com/spdx/license-list-data/v"$VER"/json/licenses.json";
  exit 1
fi
echo " Downloaded"
> "src/licenses.rs"
ENUM=""
ENUMLEN=0
echo -n "Parsing json data..."
while IFS= read -r item; do
  URL=$(echo $item | jq '.seeAlso[0]')
  if [ $URL == 'null' ]; then
      URL='"#"'
  fi
  ENUM+=",($(echo $item | jq '.licenseId'),$(echo $item | jq '.name'),$URL)"
  let "ENUMLEN+=1"
done <<< "$(echo "$CURL" | jq -c '.licenses[]')"
echo "          Done"
echo -n "Writing to file..."
sdate=$(echo "$CURL" | jq --raw-output '.releaseDate')
echo -e "// Generated $(date +'%F %T %Z')\n// Data from SPDX v$VER $sdate\n/// License list. Each entry is \`(LicenseID, LicenseName, LicenseURL)\`\npub const LICENSES: [(&str, &str, &str); $ENUMLEN] = ["${ENUM:1}"];" >> "src/manifest/licenses.rs"
echo "            Done"