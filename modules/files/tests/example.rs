use files::manifest::Manifest;

#[cfg_attr(debug_assertions, test)]
fn test_example() {
	match Manifest::from_string(include_str!("../.conic").to_string()) {
		Ok(manifest) => println!("{}", manifest),
		Err(e) => { println!("{:?}", e); assert!(false) }
	}
}