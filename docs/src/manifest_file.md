# Manifest Files

Manifest files live in the roof of an fck project and determine how the project is structured as well an define the name, version, licence, and other general things about the project.

The manifest file (`.fck`) uses a syntax similar to YAML[^1] with two main sections:
1. [`project`](#the-project-section)
2. `dependencies`

## Manifest file format

Each line (except the first) in the manifest file matches the regex string:
```regexp
^[ \t]*[^:]+(:( *.+)?|)?
```

This means we have one of three options:
1. `key`
2. `key:`
3. `key: value`

The first case is a single value on its own. The second case is a _parent_, can have _children_ under it. The last case is a simple key and value pair.

It's als worth noting that whilst the key does cannot explicitly contain spaces, no keys in the manifest file have spaces in them. You can also have multi-line strings by putting a `|` after a `:`. In this case, you write out the multi-line string under the parent as children:

```text
example-key: |
    This is a multi-line string
    This line is also part of the string
```

Indentation level is determined by the number of spaces or tabs prefixing it. One tab is equivalent to two spaces. If you keep to using one or the other, you shouldn't come across any problems with indentation.

Any time paths are given, they must be under the project root and also exist.

## The `project` section

This section contains all the project metadata such as name, authors, and description. Below are all the values you can include:

- [`name`](#name-required)
- [`src`](#src-required)
- [`tests`](#tests)
- [`benches`](#benches)
- [`kind`](#kind-required)
- [`version`](#version-required)
- [`authors`](#authors)
- [`license`](#license)
- [`description`](#description)
- [`readme`](#readme)
- [`homepage`](#homepage)
- [`repo`](#repo)
- [`features`](#features)

### `name` (required)

This is the name of the project. Names must not contain spaces.

### `src` (required)

This is the source directory of the project.

### `tests`

This is the tests directory of the project.

### `benches`

This is the benches directory of the project.

### `kind` (required)

This defines what type of package the project is. This has two child values: `lib` and `app` for library and application packages respectively. Both are a path to a file relative to the [source](#src-required) directory. You must have at least one value and can have both. Examples are given below:

```text
type:
    lib: lib.fck
```
```text
type:
    app: main.fck
    lib: lib.fck
```

### `version` (required)

This is the version of the package. This must be a [semantic version](https://semver.org/spec/v2.0.0.html) valid version.

### `authors`

The authors list is a list of authors of the package. Each author can either be a single name, or contains come general author information:
- `email`
- `gitlab`
- `github`

For example:

```text
authors:
    author1:
        author1-email@emails.com.gov.eu.net
    author2
    author3:
        gitlab: myGitLabUsername
```

### `license`

The license field is either a path to a file relative to the project root or an ID from [SPDX v3.21](https://github.com/spdx/license-list-data/tree/v3.21).

### `description`

This should give a description of the project. It shouldn't be too long and should give a brief overview of what the project does or should be used for.

### `readme`

This is a path to the README file. If not given, it assumes `README.md` to be the README if it exists.

### `homepage`

A URL to a homepage for the project. This is only really intended if your project has some separate website aside from the repo or docs.

### `repo`

URL for the repository used for the project

### `features`

This is a list of features the project uses. Each feature is either a single value or a key and value, where the key is the feature name, and the value is a description of the feature:

```text
features:
    debug
    async: Exposes asynchronous functions
```

You can also specify features to force-enable other features. This allows you to create groups of features (for example a `default` feature that contains recommended features) or ensure that correct code won't fail to compile because a feature was forgotten:

```text
features:
    dependent-feature:
        description: Example feature. Descriptions can be left out
        requires: feature1
    feature1
```

---

[^1]: The file structure is semi-custom since it requires a language specifying line
